package com.nx.biz.shopify.api.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifyFulfillmentEvent implements Serializable {

    private String shopDomain;

    private String id;
    private String adminGraphqlApiId;
    private String orderId;
    private String fulfillmentId;
    private String shopId;

    private String status;
    private String message;
    private String address1;
    private String city;
    private String province;
    private String country;
    private String zip;
    private String latitude;
    private String longitude;

    private String happenedAt;
    private String createdAt;
    private String updatedAt;
    private String estimatedDeliveryAt;

}
