package com.nx.biz.shopify.api.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifyOrder implements Serializable {

    private String shopDomain;

    private String id;
    private String adminGraphqlApiId;
    private String name;

    // 扩展字段, 快递单号, 以逗号分割的字符串
    private String trackNumber;

    private String financialStatus;
    private String fulfillmentStatus;
    private String totalLineItemsPrice;
    private String totalPrice;
    private String orderStatusUrl;

}
