package com.nx.biz.shopify.api.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifyFulfillment implements Serializable {

    private String shopDomain;

    private String id;
    private String adminGraphqlApiId;
    private String name;
    private String shopId;
    private String orderId;
    private String fulfillmentOrderId;
    private String status;
    private String shipmentStatus;
    private String service;
    private String locationId;
    private String email;
    private String trackingCompany;
    private String trackingNumber;
    private String trackingUrl;
    private String destination;
    private String items;
    private String createAt;
    private String updateAt;

    private String transferNo;
    // 0未同步 1已同步
    private Integer syncFlag;

}
