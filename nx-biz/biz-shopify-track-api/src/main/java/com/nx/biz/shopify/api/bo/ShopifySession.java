package com.nx.biz.shopify.api.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifySession implements Serializable {

    private String id;
    private String shop;
    private String state;
    private Boolean isOnline;
    private String scope;
    private Integer expires;
    private String onlineAccessInfo;
    private String accessToken;

}
