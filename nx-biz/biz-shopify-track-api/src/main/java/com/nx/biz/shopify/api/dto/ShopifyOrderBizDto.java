package com.nx.biz.shopify.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ShopifyOrderBizDto implements Serializable {

    private String id;

    private String trackCompany;
    private String trackCompanyUrl;
    private String trackNumber;

    private String zhuandanhao;

    private List<TrackEvent> trackEvents = new ArrayList<>();

    @Data
    public static class TrackEvent {
        private String id;
        private String status;
        private String message;
        private String eventTime;
    }

}
