package com.nx.biz.shopify.api.qry;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrderSyncListQry implements Serializable {

    // 空 = 所有
    // 1 = 已同步
    // 2 = 未同步
    private String syncStatus;

    // 哪个门店的查询
    private String shopDomain;

}
