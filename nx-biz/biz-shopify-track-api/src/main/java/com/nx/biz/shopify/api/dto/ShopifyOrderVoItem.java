package com.nx.biz.shopify.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifyOrderVoItem implements Serializable {

    private String id;

    private String fulfillmentId;

    private String trackCompany;
    private String trackCompanyUrl;
    private String trackNumber;

    private String transferNo;

    private String syncStatus;

    // xxx时间，到达xxx地方
    private String lastTrackStr;

}
