package com.nx.biz.shopify.api.bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SyncSettings implements Serializable {

    private String id;
    private String trackCompany;
    private String trackCompanyUrl;

}
