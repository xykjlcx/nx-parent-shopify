package com.nx.biz.shopify.api;

public class RedisKeys {
    public static final String hualeiProductList = "hualei-product-list";
    public static final String hualeiAuthInfo = "hualei-auth-info";

    public static final String shopifyFulfillment = "shopify-fulfillment";
    public static final String shopifyOrder = "shopify_order";
}
