package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

/**
 * 收据
 */
@Data
public class ReceiptBo {
    Boolean testcase;
    String authorization;
}
