package com.nx.biz.shopify.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nx.biz.shopify.model.ShopifyFulfillmentEventDo;

public interface ShopifyFulfillmentEventMapper extends BaseMapper<ShopifyFulfillmentEventDo> {


}
