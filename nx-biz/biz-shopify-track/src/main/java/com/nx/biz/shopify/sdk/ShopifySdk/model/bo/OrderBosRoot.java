package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class OrderBosRoot {
    List<OrderBoV2> orders;
}
