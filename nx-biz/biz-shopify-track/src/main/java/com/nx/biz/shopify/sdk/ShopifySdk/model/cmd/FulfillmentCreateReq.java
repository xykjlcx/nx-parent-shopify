package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import lombok.Data;

import java.util.List;

@Data
public class FulfillmentCreateReq {
    @Data
    public static class FulfillmentOrderLineItem {
        Long id;
        String quantity;
    }
    @Data
    public static class LineItemsByFulfillmentOrder {
        Long fulfillment_order_id;
        List<FulfillmentOrderLineItem> fulfillment_order_line_items;
    }
    @Data
    public static class TrackInfo {
        String number;
        String url;
        String company;
        Boolean notify_customer;
        String origin_address;
        String message;
    }

    @Data
    public static class Fulfillment {
        Long id; //update 时有用
        List<LineItemsByFulfillmentOrder> line_items_by_fulfillment_order;
        TrackInfo tracking_info;
    }

    Fulfillment fulfillment = new Fulfillment();
}
