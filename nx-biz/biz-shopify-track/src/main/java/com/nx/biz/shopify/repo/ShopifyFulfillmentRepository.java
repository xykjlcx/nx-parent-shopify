package com.nx.biz.shopify.repo;

import com.nx.biz.shopify.api.bo.ShopifyFulfillment;
import com.nx.biz.shopify.model.predicate.ShopifyFulfillmentPredicate;
import com.nx.common.base.BaseRepository;

public interface ShopifyFulfillmentRepository extends BaseRepository<ShopifyFulfillment, String, ShopifyFulfillmentPredicate> {


}
