package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class HualeiTrackNumberResp {
    @SerializedName("order_id")
    private String orderId;
    // 原单号，即客户系统订单号
    @SerializedName("order_customerinvoicecode")
    private String orderCustomerinvoicecode;

    // 参考号，可以使用该字段查询，可以理解为转单号
    @SerializedName("order_serveinvoicecode")
    private String orderServeinvoicecode;

    // 专线单号 华磊系统生成的业务单号
    @SerializedName("order_referencecode")
    private String orderReferencecode;

    // 快递类型
    @SerializedName("express_type")
    private String expressType;

    // http status
    @SerializedName("status")
    private String status;
    //http message
    @SerializedName("msg")
    private String msg;

    // 子单号？？
    @SerializedName("childno")
    private List<String> childno;
}
