package com.nx.biz.shopify.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("shopify_order")
public class ShopifyOrderDo implements Serializable {

    @TableField(value = "shop_domain")
    private String shopDomain;

    @TableId(type = IdType.INPUT)
    @TableField(value = "id")
    private String id;
    @TableField(value = "admin_graphql_api_id")
    private String adminGraphqlApiId;
    @TableField(value = "name")
    private String name;

    // 扩展字段, 快递单号, 以逗号分割的字符串
    @TableField(value = "track_number")
    private String trackNumber;

    @TableField(value = "financial_status")
    private String financialStatus;
    @TableField(value = "fulfillment_status")
    private String fulfillmentStatus;
    @TableField(value = "total_line_items_price")
    private String totalLineItemsPrice;
    @TableField(value = "total_price")
    private String totalPrice;
    @TableField(value = "order_status_url")
    private String orderStatusUrl;

}
