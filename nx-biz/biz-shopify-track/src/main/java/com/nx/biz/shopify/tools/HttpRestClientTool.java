package com.nx.biz.shopify.tools;

import com.google.gson.Gson;
import com.nx.common.result.HttpClientResult;
import com.nx.common.tools.GsonTools;
import com.nx.common.tools.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

@Slf4j
public class HttpRestClientTool {
    private static final Gson GSON = GsonTools.getGson();

    public static HttpClientResult sendGet(String url, String charset, Map<String, String> headers) throws IOException {
        log.debug("request get url: {}", url);
        log.debug("    header: {}", GSON.toJson(headers));
        HttpClientResult clientResult = HttpClientUtil.sendGet(url, charset, headers);
        log.debug("request resonse:");
        log.debug("    code: {}", clientResult.getCode());
        log.debug("    header: {}", clientResult.getResponseHeaders());
        log.debug("    content: {}", clientResult.getContent());
        return clientResult;
    }

    public static HttpClientResult sendDel(String url, String charset, Map<String, String> headers) throws IOException {
        log.debug("request del url: {}", url);
        log.debug("    header: {}", GSON.toJson(headers));
        HttpClientResult clientResult = HttpClientUtil.sendDel(url, charset, headers);
        log.debug("request resonse:");
        log.debug("    code: {}", clientResult.getCode());
        log.debug("    header: {}", clientResult.getResponseHeaders());
        log.debug("    content: {}", clientResult.getContent());
        return clientResult;
    }

    public static HttpClientResult sendPut(String url, String body, String mimeType, String charset, Map<String, String> headers) throws IOException {
        log.debug("request put url: {}", url);
        log.debug("    header: {}", GSON.toJson(headers));
        HttpClientResult clientResult = HttpClientUtil.sendPut(url, body, mimeType, charset, headers);
        log.debug("request resonse:");
        log.debug("    code: {}", clientResult.getCode());
        log.debug("    header: {}", clientResult.getResponseHeaders());
        log.debug("    content: {}", clientResult.getContent());
        return clientResult;
    }

    public static HttpClientResult post(String url, String body, String mimeType, Map<String, String> headers) throws IOException {
        log.debug("request post url: {}", url);
        log.debug("    header: {}", GSON.toJson(headers));
        log.debug("    body: {}", body);
        HttpClientResult clientResult = HttpClientUtil.post(url, body, mimeType, headers);
        log.debug("request resonse:");
        log.debug("    code: {}", clientResult.getCode());
        log.debug("    header: {}", clientResult.getResponseHeaders());
        log.debug("    content: {}", clientResult.getContent());
        return clientResult;
    }


}
