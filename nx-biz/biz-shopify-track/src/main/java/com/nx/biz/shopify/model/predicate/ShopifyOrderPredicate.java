package com.nx.biz.shopify.model.predicate;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ShopifyOrderPredicate implements Serializable {

    private String eqId;
    private List<String> notInFulfillmentStatusList = new ArrayList<>();

    private Boolean trackNumberIsNullOrEmpty;

}
