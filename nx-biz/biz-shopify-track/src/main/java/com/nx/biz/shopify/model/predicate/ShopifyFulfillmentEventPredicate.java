package com.nx.biz.shopify.model.predicate;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShopifyFulfillmentEventPredicate implements Serializable {

    private String eqId;
    private String eqFulfillmentId;

}
