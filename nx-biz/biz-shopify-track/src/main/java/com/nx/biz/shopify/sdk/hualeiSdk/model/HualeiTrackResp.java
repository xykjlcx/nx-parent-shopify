package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class HualeiTrackResp {

    @SerializedName("ack")
    private String ack;
    @SerializedName("data")
    private List<TrackItem> data;

    @NoArgsConstructor
    @Data
    public static class TrackItem {
        // 跟踪号 tracking number
        @SerializedName("trackingNumber")
        private String trackingNumber;
        // 收件国家代码 country code: CN
        @SerializedName("consigneeCountry")
        private String consigneeCountry;
        // 物流方式 express type: 0528 广州E邮宝
        @SerializedName("productKindName")
        private String productKindName;
        // 参考号
        @SerializedName("referenceNumber")
        private String referenceNumber;

        // 末条轨迹内容
        @SerializedName("trackContent")
        private String trackContent;
        // 末条轨迹时间
        @SerializedName("trackDate")
        private String trackDate;
        // 末条轨迹位置
        @SerializedName("trackLocation")
        private String trackLocation;
        // 签收人 sign person,if no sign it's empty
        @SerializedName("trackSignperson")
        private String trackSignperson;
        // 轨迹明细
        @SerializedName("trackDetails")
        private List<TrackDetailsDTO> trackDetails;

        @NoArgsConstructor
        @Data
        public static class TrackDetailsDTO {
            @SerializedName("track_content")
            private String trackContent;
            @SerializedName("track_date")
            private String trackDate;
            @SerializedName("track_location")
            private String trackLocation;
            // 签收日期
            @SerializedName("track_signdate")
            private String trackSigndate;
            // 签收时间
            @SerializedName("track_signperson")
            private String trackSignperson;
        }
    }
}
