package com.nx.biz.shopify.repo;

import com.nx.biz.shopify.api.bo.ShopifyFulfillmentEvent;
import com.nx.biz.shopify.model.predicate.ShopifyFulfillmentEventPredicate;
import com.nx.common.base.BaseRepository;

public interface ShopifyFulfillmentEventRepository extends BaseRepository<ShopifyFulfillmentEvent, String, ShopifyFulfillmentEventPredicate> {


}
