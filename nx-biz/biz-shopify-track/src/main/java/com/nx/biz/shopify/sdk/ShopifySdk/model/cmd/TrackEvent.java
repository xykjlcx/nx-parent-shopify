package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import cn.hutool.core.lang.Assert;
import com.nx.common.tools.CheckTools;
import lombok.Data;

import java.util.List;

@Data
public class TrackEvent {
    String orderId;
    String trackingNumber;
    private List<TrackEventInfo> trackEventInfoList;

    public void check() {
        Assert.isTrue(CheckTools.isNotNullOrEmpty(orderId), "订单id为空");
        Assert.isTrue(CheckTools.isNotNullOrEmpty(trackingNumber), "trackingNumber为空");
    }
}
