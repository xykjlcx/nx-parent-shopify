package com.nx.biz.shopify.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nx.biz.shopify.model.ShopifySessionDo;

public interface ShopifySessionMapper extends BaseMapper<ShopifySessionDo> {


}
