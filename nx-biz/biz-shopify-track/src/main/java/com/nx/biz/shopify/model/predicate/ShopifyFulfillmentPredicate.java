package com.nx.biz.shopify.model.predicate;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ShopifyFulfillmentPredicate implements Serializable {

    private String eqId;
    private String eqOrderId;
    private String eqTrackNumber;
    private String neStatus;

    private List<String> inStatusList = new ArrayList<>();

}
