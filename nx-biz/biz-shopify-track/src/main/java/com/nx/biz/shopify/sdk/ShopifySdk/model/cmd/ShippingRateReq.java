package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class ShippingRateReq {

    @SerializedName("rate")
    private RateReqDTO rate;

    @NoArgsConstructor
    @Data
    public static class RateReqDTO {
        @SerializedName("origin")
        private AddressDTO origin;
        @SerializedName("destination")
        private AddressDTO destination;
        @SerializedName("items")
        private List<ItemsDTO> items;
        @SerializedName("currency")
        private String currency;
        @SerializedName("locale")
        private String locale;

        @NoArgsConstructor
        @Data
        public static class AddressDTO {
            @SerializedName("country")
            private String country;
            @SerializedName("postal_code")
            private String postalCode;
            @SerializedName("province")
            private String province;
            @SerializedName("city")
            private String city;
            @SerializedName("name")
            private Object name;
            @SerializedName("address1")
            private String address1;
            @SerializedName("address2")
            private String address2;
            @SerializedName("address3")
            private Object address3;
            @SerializedName("phone")
            private Object phone;
            @SerializedName("fax")
            private Object fax;
            @SerializedName("email")
            private Object email;
            @SerializedName("address_type")
            private Object addressType;
            @SerializedName("company_name")
            private String companyName;
        }

        @NoArgsConstructor
        @Data
        public static class ItemsDTO {
            @SerializedName("name")
            private String name;
            @SerializedName("sku")
            private String sku;
            @SerializedName("quantity")
            private Integer quantity;
            @SerializedName("grams")
            private Integer grams;
            @SerializedName("price")
            private Integer price;
            @SerializedName("vendor")
            private String vendor;
            @SerializedName("requires_shipping")
            private Boolean requiresShipping;
            @SerializedName("taxable")
            private Boolean taxable;
            @SerializedName("fulfillment_service")
            private String fulfillmentService;
            @SerializedName("properties")
            private Object properties;
            @SerializedName("product_id")
            private Long productId;
            @SerializedName("variant_id")
            private Long variantId;
        }
    }
}
