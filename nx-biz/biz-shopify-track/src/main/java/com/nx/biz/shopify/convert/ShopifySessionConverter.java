package com.nx.biz.shopify.convert;

import com.nx.biz.shopify.api.bo.ShopifySession;
import com.nx.biz.shopify.model.ShopifySessionDo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShopifySessionConverter {

    public ShopifySession toBo(ShopifySessionDo doItem) {
        ShopifySession boItem = new ShopifySession();
        BeanUtils.copyProperties(doItem, boItem);
        return boItem;
    }

    public ShopifySessionDo toDo(ShopifySession boItem) {
        ShopifySessionDo doItem = new ShopifySessionDo();
        BeanUtils.copyProperties(boItem, doItem);
        return doItem;
    }

    public List<ShopifySession> toBoList(List<ShopifySessionDo> doList) {
        List<ShopifySession> boList = new ArrayList<>();
        for (ShopifySessionDo doItem : doList) {
            ShopifySession boItem = this.toBo(doItem);
            // 加入list
            boList.add(boItem);
        }
        return boList;
    }

    public List<ShopifySessionDo> toDoList(List<ShopifySession> boList) {
        List<ShopifySessionDo> doList = new ArrayList<>();
        for (ShopifySession boItem : boList) {
            ShopifySessionDo doItem = this.toDo(boItem);
            // 加入list
            doList.add(doItem);
        }
        return doList;
    }

}
