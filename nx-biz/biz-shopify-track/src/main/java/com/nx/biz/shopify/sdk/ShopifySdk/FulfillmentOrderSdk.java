package com.nx.biz.shopify.sdk.ShopifySdk;

import com.google.gson.Gson;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentOrderBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentOrderServiceListBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.constant.UrlConstant;
import com.nx.biz.shopify.tools.HttpRestClientTool;
import com.nx.common.result.HttpClientResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FulfillmentOrderSdk {

    protected static final Gson GSON = new Gson();
//    public static String urlTemplate = "https://%s.myshopify.com/admin/api/2024-04/assigned_fulfillment_orders.json";
//    public static String acceptUrlTemplate = "https://%s.myshopify.com/admin/api/2024-04/fulfillment_orders/{fulfillment_order_id}/fulfillment_request/accept.json";
//    public static String rejectUrlTemplate = "https://%s.myshopify.com/admin/api/2024-04/fulfillment_orders/{fulfillment_order_id}/fulfillment_request/reject.json";
//
//    public static String orderFulfillmentUrl = "https://%s.myshopify.com/admin/api/2024-04/orders/%s/fulfillment_orders.json";
//
//    public static String token = "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";
//    public static String devStoreNo = "track-dev-0704";

    public List<FulfillmentOrderBo> listAssignFulfillmentOrderByOrderId(Long orderId, String shopDomain, String apiVersion, String accessToken) throws IOException {

        /** 1. 参数初始化 */
//        String url = String.format(orderFulfillmentUrl, devStoreNo, orderId);
        String url = String.format(UrlConstant.FulfillmentOrderRestUrl, shopDomain, apiVersion, orderId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("resp content: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentOrderServiceListBo fulfillmentOrderServiceListBo = GSON.fromJson(content, FulfillmentOrderServiceListBo.class);
        return fulfillmentOrderServiceListBo.getFulfillment_orders();
    }


    public List<FulfillmentOrderBo> listAssignFulfillmentOrder(String location_ids, String assignment_status,
                                                               String shopDomain, String apiVersion, String accessToken) throws Exception {

        /** 1. 参数初始化 */
//        String url = String.format(urlTemplate, devStoreNo);
        String url = String.format(UrlConstant.FulfillmentOrderRestUrl, shopDomain, apiVersion);
        URI uri = new URIBuilder(url)
                .setParameter("assignment_status", assignment_status)
                .setParameter("location_ids[]", location_ids)
                .build();

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(uri.toString(), "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("queryFulfillmentOrder resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentOrderServiceListBo fulfillmentOrderServiceListBo = GSON.fromJson(content, FulfillmentOrderServiceListBo.class);
        return fulfillmentOrderServiceListBo.getFulfillment_orders();
    }

    public void acceptFulfillmentOrder(String fulfillment_order_id, String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(acceptUrlTemplate, devStoreNo, fulfillment_order_id);
        String url = String.format(UrlConstant.FulfillmentOrderAcceptRestUrl, shopDomain, apiVersion);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        HashMap<String, Object> body = new HashMap<>();
        body.put("id", fulfillment_order_id);
        body.put("message", "");

        /** 2. 发送请求*/
        HttpClientResult result = HttpRestClientTool.post(url, GSON.toJson(body), "application/json", headers);
        if (result.getCode() != 201) {
            log.error("accept fulfillmentOrder失败, {}", result);
        }

        /** 3. 解析返回值 */

    }

    public void rejectFulfillmentOrder(String devStoreNo, String fulfillment_order_id, String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(rejectUrlTemplate, devStoreNo, fulfillment_order_id);
        String url = String.format(UrlConstant.FulfillmentOrderRejectRestUrl, shopDomain, apiVersion);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        HashMap<String, Object> body = new HashMap<>();
        body.put("id", fulfillment_order_id);
        body.put("message", "");
        body.put("reason", "");

        /** 2. 发送请求*/
        HttpClientResult result = HttpRestClientTool.post(url, GSON.toJson(body), "application/json", headers);
        if (result.getCode() != 201) {
            log.error("reject fulfillmentOrder失败, {}", result);
        }

        /** 3. 解析返回值 */

    }
}
