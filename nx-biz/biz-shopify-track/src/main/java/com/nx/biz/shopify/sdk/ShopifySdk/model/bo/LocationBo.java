package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class LocationBo {
    Long id;
    String address1;
    String address2;
    String city;
    String company;
    String country;
    String email;
    String first_name;
    String last_name;
    String phone;
    String province;
    String zip;
}
