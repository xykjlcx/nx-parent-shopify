package com.nx.biz.shopify.repo.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nx.biz.shopify.api.bo.ShopifyFulfillment;
import com.nx.biz.shopify.convert.ShopifyFulfillmentConverter;
import com.nx.biz.shopify.mapper.ShopifyFulfillmentMapper;
import com.nx.biz.shopify.model.ShopifyFulfillmentDo;
import com.nx.biz.shopify.model.predicate.ShopifyFulfillmentPredicate;
import com.nx.biz.shopify.repo.ShopifyFulfillmentRepository;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Slf4j
@Repository
public class ShopifyFulfillmentRepoImpl implements ShopifyFulfillmentRepository {

    @Resource
    private ShopifyFulfillmentMapper shopifyFulfillmentMapper;
    @Resource
    private ShopifyFulfillmentConverter shopifyFulfillmentConverter;

    // 将抽象的查询条件，转化为mybatis plus的检索参数
    private LambdaQueryWrapper<ShopifyFulfillmentDo> buildQueryWrapper(ShopifyFulfillmentPredicate predicate) {
        LambdaQueryWrapper<ShopifyFulfillmentDo> queryWrapper = new LambdaQueryWrapper<>();
        if (CheckTools.isNullOrEmpty(predicate)) {
            return queryWrapper;
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqId())) {
            queryWrapper.eq(ShopifyFulfillmentDo::getId, predicate.getEqId());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqOrderId())) {
            queryWrapper.eq(ShopifyFulfillmentDo::getOrderId, predicate.getEqOrderId());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqTrackNumber())) {
            queryWrapper.eq(ShopifyFulfillmentDo::getTrackingNumber, predicate.getEqTrackNumber());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getNeStatus())) {
            queryWrapper.ne(ShopifyFulfillmentDo::getStatus, predicate.getNeStatus());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getInStatusList())) {
            queryWrapper.in(ShopifyFulfillmentDo::getStatus, predicate.getInStatusList());
        }
        return queryWrapper;
    }

    @Override
    public String save(ShopifyFulfillment shopifyFulfillmentBo) {
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentBo)) {
            throw new RuntimeException("shopifyFulfillmentBo is null");
        }
        ShopifyFulfillmentDo fulfillmentConverterDo = shopifyFulfillmentConverter.toDo(shopifyFulfillmentBo);
        if (CheckTools.isNullOrEmpty(fulfillmentConverterDo.getId())) {
            // 如果id业务没有设置，则生成一个
            fulfillmentConverterDo.setId(IdUtil.simpleUUID());
        }
        // 调用mybatis落库
        shopifyFulfillmentMapper.insert(fulfillmentConverterDo);
        return fulfillmentConverterDo.getId();
    }

    @Override
    public void update(ShopifyFulfillment shopifyFulfillmentBo) {
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentBo)) {
            throw new RuntimeException("shopifyFulfillmentBo is null");
        }
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentBo.getId())) {
            throw new RuntimeException("shopifyFulfillmentBo.id is null, can not update shopifyFulfillmentBo without id");
        }
        // 由于是业务对象映射到数据对象，所以这一步一定要保证没有信息的丢失
        ShopifyFulfillmentDo fulfillmentConverterDo = shopifyFulfillmentConverter.toDo(shopifyFulfillmentBo);
        // 调用mybatis落库
        shopifyFulfillmentMapper.updateById(fulfillmentConverterDo);
    }

    @Override
    public void deleteById(String dbId) {
        shopifyFulfillmentMapper.deleteById(dbId);
    }

    @Override
    public ShopifyFulfillment queryById(String dbId) {
        ShopifyFulfillmentDo shopifyFulfillmentDo = shopifyFulfillmentMapper.selectById(dbId);
        if (CheckTools.isNotNullOrEmpty(shopifyFulfillmentDo)) {
            ShopifyFulfillment fulfillmentConverterBo = shopifyFulfillmentConverter.toBo(shopifyFulfillmentDo);
            return fulfillmentConverterBo;
        }
        return null;
    }

    @Override
    public List<ShopifyFulfillment> queryList(ShopifyFulfillmentPredicate predicate) {
        LambdaQueryWrapper<ShopifyFulfillmentDo> lambdaQueryWrapper = this.buildQueryWrapper(predicate);
        List<ShopifyFulfillmentDo> shopifyFulfillmentDos = shopifyFulfillmentMapper.selectList(lambdaQueryWrapper);
        if (CheckTools.isNotNullOrEmpty(shopifyFulfillmentDos)) {
            // 映射为boList，这里是否抽离一层逻辑单独实现
            return shopifyFulfillmentConverter.toBoList(shopifyFulfillmentDos);
        }
        return Collections.emptyList();
    }

}
