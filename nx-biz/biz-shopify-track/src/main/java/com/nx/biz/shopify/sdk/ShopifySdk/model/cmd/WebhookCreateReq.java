package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import lombok.Data;

@Data
public class WebhookCreateReq {
    @Data
    public static class WebhookCreateReqA {
        String topic;
        String address;
        String format = "json";
    }
    WebhookCreateReqA webhook = new WebhookCreateReqA();
}
