package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class TaxLineBo {
    @Data
    public class PriceSet {
        private MoneyBo shop_money;
        private MoneyBo presentment_money;
    }

    private String title;
    private String price;
    private double rate;
    private PriceSet price_set;
}
