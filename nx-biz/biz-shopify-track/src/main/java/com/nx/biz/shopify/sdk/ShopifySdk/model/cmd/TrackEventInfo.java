package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TrackEventInfo {
    // 状态
    private String status;
    // 消息
    private String message;
    //位置
    private String location;
    //发生时间
    private String time;


    //经度
    private String longitude;
    //维度
    private String latitude;
    private String zip;
    private String city;
    private String province;
    private String country;
}
