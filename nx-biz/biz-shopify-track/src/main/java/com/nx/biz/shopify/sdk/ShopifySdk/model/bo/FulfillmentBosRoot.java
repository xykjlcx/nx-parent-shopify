package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class FulfillmentBosRoot {
    @SerializedName("fulfillments")
    List<FulfillmentBo> fulfillments;
}
