package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class HualeiOrderCreateReq {

    @SerializedName("buyerid")
    private String buyerid;
    @SerializedName("order_piece")
    private String orderPiece;
    @SerializedName("consignee_mobile")
    private String consigneeMobile;
    @SerializedName("order_returnsign")
    private String orderReturnsign;
    @SerializedName("trade_type")
    private String tradeType;
    @SerializedName("duty_type")
    private String dutyType;
    @SerializedName("battery_type")
    private String batteryType;
    @SerializedName("consignee_name")
    private String consigneeName;
    @SerializedName("consignee_companyname")
    private String consigneeCompanyname;
    @SerializedName("consignee_address")
    private String consigneeAddress;
    @SerializedName("consignee_telephone")
    private String consigneeTelephone;
    @SerializedName("country")
    private String country;
    @SerializedName("consignee_state")
    private String consigneeState;
    @SerializedName("consignee_city")
    private String consigneeCity;
    @SerializedName("consignee_suburb")
    private String consigneeSuburb;
    @SerializedName("consignee_postcode")
    private String consigneePostcode;
    @SerializedName("consignee_passportno")
    private String consigneePassportno;
    @SerializedName("consignee_email")
    private String consigneeEmail;
    @SerializedName("consignee_taxno")
    private String consigneeTaxno;
    @SerializedName("consignee_taxnotype")
    private String consigneeTaxnotype;
    @SerializedName("consignee_taxnocountry")
    private String consigneeTaxnocountry;
    @SerializedName("consignee_streetno")
    private String consigneeStreetno;
    @SerializedName("consignee_doorno")
    private String consigneeDoorno;
    @SerializedName("shipper_taxnotype")
    private String shipperTaxnotype;
    @SerializedName("shipper_taxno")
    private String shipperTaxno;
    @SerializedName("shipper_taxnocountry")
    private String shipperTaxnocountry;
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_userid")
    private String customerUserid;
    @SerializedName("order_customerinvoicecode")
    private String orderCustomerinvoicecode;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("weight")
    private String weight;
    @SerializedName("product_imagepath")
    private String productImagepath;
    @SerializedName("order_transactionurl")
    private String orderTransactionurl;
    @SerializedName("order_cargoamount")
    private String orderCargoamount;
    @SerializedName("order_insurance")
    private String orderInsurance;
    @SerializedName("cargo_type")
    private String cargoType;
    @SerializedName("order_customnote")
    private String orderCustomnote;
    @SerializedName("orderInvoiceParam")
    private List<OrderInvoiceParamDTO> orderInvoiceParam;
    @SerializedName("orderVolumeParam")
    private List<OrderVolumeParamDTO> orderVolumeParam;

    @NoArgsConstructor
    @Data
    public static class OrderInvoiceParamDTO {
        @SerializedName("invoice_amount")
        private String invoiceAmount;
        @SerializedName("invoice_pcs")
        private String invoicePcs;
        @SerializedName("invoice_title")
        private String invoiceTitle;
        @SerializedName("invoice_weight")
        private String invoiceWeight;
        @SerializedName("sku")
        private String sku;
        @SerializedName("sku_code")
        private String skuCode;
        @SerializedName("hs_code")
        private String hsCode;
        @SerializedName("import_hs_code")
        private String importHsCode;
        @SerializedName("transaction_url")
        private String transactionUrl;
        @SerializedName("invoiceunit_code")
        private String invoiceunitCode;
        @SerializedName("invoice_imgurl")
        private String invoiceImgurl;
        @SerializedName("invoice_brand")
        private String invoiceBrand;
        @SerializedName("invoice_rule")
        private String invoiceRule;
        @SerializedName("invoice_currency")
        private String invoiceCurrency;
        @SerializedName("invoice_taxno")
        private String invoiceTaxno;
        @SerializedName("origin_country")
        private String originCountry;
        @SerializedName("invoice_material")
        private String invoiceMaterial;
        @SerializedName("invoice_purpose")
        private String invoicePurpose;
    }

    @NoArgsConstructor
    @Data
    public static class OrderVolumeParamDTO {
        @SerializedName("volume_height")
        private String volumeHeight;
        @SerializedName("volume_length")
        private String volumeLength;
        @SerializedName("volume_width")
        private String volumeWidth;
        @SerializedName("volume_weight")
        private String volumeWeight;
    }
}
