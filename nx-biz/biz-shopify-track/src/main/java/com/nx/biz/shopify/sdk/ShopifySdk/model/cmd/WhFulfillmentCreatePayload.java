package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.LineItemBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.ReceiptBo;
import lombok.Data;

import javax.print.attribute.standard.Destination;
import java.util.List;

@Data
public class WhFulfillmentCreatePayload {
    private long id;
    private long order_id;
    private String status;
    private String service;
    private String tracking_company;
    private String shipment_status;
    private String location_id;
    private String origin_address;
    private String email;
    private Destination destination;
    private List<LineItemBo> line_items;
    private String tracking_number;
    private List<String> tracking_numbers;
    private String tracking_url;
    private List<String> tracking_urls;
    private ReceiptBo receipt;
    private String name;
    private String admin_graphql_api_id;

    private String created_at;
    private String updated_at;
}
