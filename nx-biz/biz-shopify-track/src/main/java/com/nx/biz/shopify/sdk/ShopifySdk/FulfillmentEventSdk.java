package com.nx.biz.shopify.sdk.ShopifySdk;

import com.google.gson.Gson;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentEventBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentEventBoRoot;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentEventBosRoot;
import com.nx.biz.shopify.sdk.ShopifySdk.model.constant.UrlConstant;
import com.nx.biz.shopify.tools.HttpRestClientTool;
import com.nx.common.result.HttpClientResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FulfillmentEventSdk {

    //    public static String urlTemplate = "https://%s.myshopify.com/admin/api/2024-07/orders/%s/fulfillments/%s/events.json";
//    public static String urlInstTemplate = "https://%s.myshopify.com/admin/api/2024-07/orders/%s/fulfillments/%s/events/%s.json";
//
//    public static String token = "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";
//    public static String devStoreNo = "track-dev-0704";
    protected static final Gson GSON = new Gson();

    public FulfillmentEventBo createFulfillmentEvent(Long orderId, Long fulfillmentId, FulfillmentEventBo fulfillmentEventBo,
                                                     String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = shopifyContext.buildFullUrl(UrlConstant.FulfillmentEventRestUrl);
        String url = String.format(UrlConstant.FulfillmentEventRestUrl, shopDomain, apiVersion, orderId, fulfillmentId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        Map<String, Object> reqContent = new HashMap<>();
        reqContent.put("event", fulfillmentEventBo);
        HttpClientResult clientResult = HttpRestClientTool.post(url, GSON.toJson(reqContent), "application/json", headers);
        if (clientResult.getCode() != 201) {
            log.error("createFulfillmentEvent resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentEventBoRoot fulfillmentEventBoRoot = GSON.fromJson(content, FulfillmentEventBoRoot.class);
        return fulfillmentEventBoRoot.getFulfillmentEvent();
    }

    public List<FulfillmentEventBo> listFulfillmentEvent(Long orderId, Long fulfillmentId,
                                                         String shopDomain, String apiVersion, String accessToken) throws Exception {
        /** 1. 参数初始化 */
//        String url = String.format(urlTemplate, devStoreNo, orderId, fulfillmentId);
        String url = String.format(UrlConstant.FulfillmentEventRestUrl, shopDomain, apiVersion, orderId, fulfillmentId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("listAssignFulfillmentOrderByOrderId resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentEventBosRoot fulfillmentEventBosRoot = GSON.fromJson(content, FulfillmentEventBosRoot.class);
        return fulfillmentEventBosRoot.getFulfillmentEvents();
    }

    public FulfillmentEventBo getFulfillmentEvent(Long orderId, Long fulfillmentId, Long fulfillmentEventId,
                                                  String shopDomain, String apiVersion, String accessToken) throws Exception {
        /** 1. 参数初始化 */
//        String url = String.format(urlInstTemplate, devStoreNo, orderId, fulfillmentId, fulfillmentEventId);
        String url = String.format(UrlConstant.FulfillmentEventInstRestUrl, shopDomain, apiVersion, orderId, fulfillmentId, fulfillmentEventId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("listAssignFulfillmentOrderByOrderId resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentEventBoRoot fulfillmentEventBoRoot = GSON.fromJson(content, FulfillmentEventBoRoot.class);
        return fulfillmentEventBoRoot.getFulfillmentEvent();
    }

    public String deleteFulfillmentEvent(Long orderId, Long fulfillmentId, Long fulfillmentEventId,
                                         String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(urlInstTemplate, devStoreNo, orderId, fulfillmentId, fulfillmentEventId);
        String url = String.format(UrlConstant.FulfillmentEventInstRestUrl, shopDomain, apiVersion, orderId, fulfillmentId, fulfillmentEventId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendDel(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("listAssignFulfillmentOrderByOrderId resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        return content;
    }
}
