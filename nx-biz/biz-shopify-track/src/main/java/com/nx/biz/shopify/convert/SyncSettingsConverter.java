package com.nx.biz.shopify.convert;

import com.nx.biz.shopify.api.bo.SyncSettings;
import com.nx.biz.shopify.model.SyncSettingsDo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SyncSettingsConverter {

    public SyncSettings toBo(SyncSettingsDo doItem) {
        SyncSettings boItem = new SyncSettings();
        BeanUtils.copyProperties(doItem, boItem);
        return boItem;
    }

    public SyncSettingsDo toDo(SyncSettings boItem) {
        SyncSettingsDo doItem = new SyncSettingsDo();
        BeanUtils.copyProperties(boItem, doItem);
        return doItem;
    }

    public List<SyncSettings> toBoList(List<SyncSettingsDo> doList) {
        List<SyncSettings> boList = new ArrayList<>();
        for (SyncSettingsDo doItem : doList) {
            SyncSettings boItem = this.toBo(doItem);
            // 加入list
            boList.add(boItem);
        }
        return boList;
    }

    public List<SyncSettingsDo> toDoList(List<SyncSettings> boList) {
        List<SyncSettingsDo> doList = new ArrayList<>();
        for (SyncSettings boItem : boList) {
            SyncSettingsDo doItem = this.toDo(boItem);
            // 加入list
            doList.add(doItem);
        }
        return doList;
    }

}
