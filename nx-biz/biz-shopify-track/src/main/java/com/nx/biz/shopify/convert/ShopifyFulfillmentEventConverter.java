package com.nx.biz.shopify.convert;

import com.nx.biz.shopify.api.bo.ShopifyFulfillmentEvent;
import com.nx.biz.shopify.model.ShopifyFulfillmentEventDo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentEventBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.cmd.TrackEvent;
import com.nx.biz.shopify.sdk.ShopifySdk.model.cmd.TrackEventInfo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.constant.ShipmentStatusConstants;
import com.nx.biz.shopify.sdk.hualeiSdk.model.HualeiTrackResp;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ShopifyFulfillmentEventConverter {

    public ShopifyFulfillmentEvent toBo(ShopifyFulfillmentEventDo doItem) {
        ShopifyFulfillmentEvent boItem = new ShopifyFulfillmentEvent();
        BeanUtils.copyProperties(doItem, boItem);
        return boItem;
    }

    public ShopifyFulfillmentEventDo toDo(ShopifyFulfillmentEvent boItem) {
        ShopifyFulfillmentEventDo doItem = new ShopifyFulfillmentEventDo();
        BeanUtils.copyProperties(boItem, doItem);
        return doItem;
    }

    public List<ShopifyFulfillmentEvent> toBoList(List<ShopifyFulfillmentEventDo> doList) {
        List<ShopifyFulfillmentEvent> boList = new ArrayList<>();
        for (ShopifyFulfillmentEventDo doItem : doList) {
            ShopifyFulfillmentEvent boItem = this.toBo(doItem);
            // 加入list
            boList.add(boItem);
        }
        return boList;
    }

    public List<ShopifyFulfillmentEventDo> toDoList(List<ShopifyFulfillmentEvent> boList) {
        List<ShopifyFulfillmentEventDo> doList = new ArrayList<>();
        for (ShopifyFulfillmentEvent boItem : boList) {
            ShopifyFulfillmentEventDo doItem = this.toDo(boItem);
            // 加入list
            doList.add(doItem);
        }
        return doList;
    }

    /* ------------------------------------------------------------------------------------ */

    // 对照所有6月份的已完成的运单整理如下状态
    public String toShopifyStatus(String content) {

        if (content.contains("Shipment information sent to")
                || content.contains("The instruction data for this shipment have been provided by the sender to DHL electronically.")
                || content.contains("Order information has been transmitted")
                || content.contains("Order information has been transmitted to")
                || content.contains("创建标签")
                || content.contains("Shipper created a label")
        ) {
            return ShipmentStatusConstants.label_printed;
        } else if (content.contains("取件扫描")
                || (content.contains("Picked up")
                || (content.contains("货物离开操作中心")))) {
            return ShipmentStatusConstants.picked_up;
        } else if ((content.contains("for delivery"))
                || (content.contains("The shipment has been loaded onto the delivery vehicle"))
                || (content.contains("Your local courier will deliver your parcel"))
                || content.contains("Out For Delivery")
                || content.contains("vehicle for delivery")
                || content.contains("On FedEx vehicle for delivery")
                || content.contains("Being delivered")
                || content.contains("Item in delivery")
                || content.contains("Out for delivery")
        ) {
            return ShipmentStatusConstants.out_for_delivery;
        } else if (content.contains("已递送")
                || content.contains("Delivered")
                || (content.contains("The shipment has been successfully delivered"))
                || content.contains("We've delivered your parcel to the delivery address")
                || content.contains("We've delivered this parcel to the recipients porch")
                || content.contains("DELIVERED")
                || content.contains("Delivery successful")
                || content.contains("Entregado")
        ) {
            return ShipmentStatusConstants.delivered;
        } else {
            return ShipmentStatusConstants.in_transit;
        }
    }

    public TrackEvent toTrackEvent(String orderId, HualeiTrackResp.TrackItem trackItem) {
        TrackEvent trackEvent = new TrackEvent();
        trackEvent.setTrackEventInfoList(new ArrayList<>());

        trackEvent.setTrackingNumber(trackItem.getTrackingNumber());
        trackEvent.setOrderId(orderId);

        for (HualeiTrackResp.TrackItem.TrackDetailsDTO trackDetailsDTO : trackItem.getTrackDetails()) {
            TrackEventInfo trackEventInfo = new TrackEventInfo();

            trackEventInfo.setLocation(trackDetailsDTO.getTrackLocation());
            String shopifyStatus = toShopifyStatus(trackDetailsDTO.getTrackContent());
            trackEventInfo.setStatus(shopifyStatus);
            trackEventInfo.setMessage(trackDetailsDTO.getTrackContent());
            trackEventInfo.setTime(trackDetailsDTO.getTrackDate());

            if (CheckTools.isNotNullOrEmpty(trackDetailsDTO.getTrackSigndate())) {
                trackEventInfo.setStatus(ShipmentStatusConstants.delivered);
            }

            trackEvent.getTrackEventInfoList().add(trackEventInfo);
        }

        log.info("翻译后的track event : {}", trackEvent);
        return trackEvent;
    }

    public FulfillmentEventBo trackEventInfo2ShopifyEvent(TrackEventInfo trackEventInfo) {
        FulfillmentEventBo fulfillmentEventBo = new FulfillmentEventBo();
        fulfillmentEventBo.setStatus(trackEventInfo.getStatus());   // todo , 状态之间有一个转换。
        fulfillmentEventBo.setAddress1(trackEventInfo.getLocation());
        fulfillmentEventBo.setHappenedAt(trackEventInfo.getTime());
        if (CheckTools.isNotNullOrEmpty(trackEventInfo.getLongitude())) {
            fulfillmentEventBo.setLongitude(Double.valueOf(trackEventInfo.getLongitude()));
        }
        if (CheckTools.isNotNullOrEmpty(trackEventInfo.getLatitude())) {
            fulfillmentEventBo.setLatitude(Double.valueOf(trackEventInfo.getLatitude()));
        }
        fulfillmentEventBo.setCountry(trackEventInfo.getCountry());
        fulfillmentEventBo.setProvince(trackEventInfo.getProvince());
        fulfillmentEventBo.setCity(trackEventInfo.getCity());
        fulfillmentEventBo.setZip(trackEventInfo.getZip());
        fulfillmentEventBo.setMessage(trackEventInfo.getMessage());

        return fulfillmentEventBo;
    }

}
