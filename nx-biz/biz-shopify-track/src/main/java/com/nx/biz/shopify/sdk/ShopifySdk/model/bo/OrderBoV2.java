package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class OrderBoV2 {
    @SerializedName("id")
    private Long id;
    @SerializedName("admin_graphql_api_id")
    private String adminGraphqlApiId;

    @SerializedName("name")
    private String name;

    // constant OrderFinancialStatus
    @SerializedName("financial_status")
    private String financialStatus;
    @SerializedName("fulfillment_status")
    private String fulfillmentStatus;
    @SerializedName("total_line_items_price")
    private String totalLineItemsPrice;
    @SerializedName("total_price")
    private String totalPrice;

    List<FulfillmentBo> fulfillments;
}
