package com.nx.biz.shopify.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nx.biz.shopify.model.ShopifyFulfillmentDo;

public interface ShopifyFulfillmentMapper extends BaseMapper<ShopifyFulfillmentDo> {


}
