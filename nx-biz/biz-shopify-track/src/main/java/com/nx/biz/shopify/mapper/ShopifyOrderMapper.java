package com.nx.biz.shopify.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.nx.biz.shopify.model.ShopifyOrderDo;

public interface ShopifyOrderMapper extends BaseMapper<ShopifyOrderDo> {


}
