package com.nx.biz.shopify.repo.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nx.biz.shopify.api.bo.ShopifySession;
import com.nx.biz.shopify.convert.ShopifySessionConverter;
import com.nx.biz.shopify.mapper.ShopifySessionMapper;
import com.nx.biz.shopify.model.ShopifySessionDo;
import com.nx.biz.shopify.model.predicate.ShopifySessionPredicate;
import com.nx.biz.shopify.repo.ShopifySessionRepository;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Slf4j
@Repository
public class ShopifySessionRepoImpl implements ShopifySessionRepository {

    @Resource
    private ShopifySessionMapper shopifySessionMapper;
    @Resource
    private ShopifySessionConverter converter;

    // 将抽象的查询条件，转化为mybatis plus的检索参数
    private LambdaQueryWrapper<ShopifySessionDo> buildQueryWrapper(ShopifySessionPredicate predicate) {
        LambdaQueryWrapper<ShopifySessionDo> queryWrapper = new LambdaQueryWrapper<>();
        if (CheckTools.isNullOrEmpty(predicate)) {
            return queryWrapper;
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqId())) {
            queryWrapper.eq(ShopifySessionDo::getId, predicate.getEqId());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqShop())) {
            queryWrapper.eq(ShopifySessionDo::getShop, predicate.getEqShop());
        }
        return queryWrapper;
    }

    @Override
    public String save(ShopifySession shopifySessionBo) {
        if (CheckTools.isNullOrEmpty(shopifySessionBo)) {
            throw new RuntimeException("shopifySessionBo is null");
        }
        ShopifySessionDo shopifySessionDo = converter.toDo(shopifySessionBo);
        if (CheckTools.isNullOrEmpty(shopifySessionDo.getId())) {
            // 如果id业务没有设置，则生成一个
            shopifySessionDo.setId(IdUtil.simpleUUID());
        }
        // 调用mybatis落库
        shopifySessionMapper.insert(shopifySessionDo);
        return shopifySessionDo.getId();
    }

    @Override
    public void update(ShopifySession shopifySessionBo) {
        if (CheckTools.isNullOrEmpty(shopifySessionBo)) {
            throw new RuntimeException("shopifySessionBo is null");
        }
        if (CheckTools.isNullOrEmpty(shopifySessionBo.getId())) {
            throw new RuntimeException("shopifySessionBo.id is null, can not update shopifySessionBo without id");
        }
        // 由于是业务对象映射到数据对象，所以这一步一定要保证没有信息的丢失
        ShopifySessionDo shopifySessionDo = converter.toDo(shopifySessionBo);
        // 调用mybatis落库
        shopifySessionMapper.updateById(shopifySessionDo);
    }

    @Override
    public void deleteById(String dbId) {
        shopifySessionMapper.deleteById(dbId);
    }

    @Override
    public ShopifySession queryById(String dbId) {
        ShopifySessionDo shopifySessionDo = shopifySessionMapper.selectById(dbId);
        if (CheckTools.isNotNullOrEmpty(shopifySessionDo)) {
            ShopifySession shopifySessionBo = converter.toBo(shopifySessionDo);
            return shopifySessionBo;
        }
        return null;
    }

    @Override
    public List<ShopifySession> queryList(ShopifySessionPredicate predicate) {
        LambdaQueryWrapper<ShopifySessionDo> lambdaQueryWrapper = this.buildQueryWrapper(predicate);
        List<ShopifySessionDo> shopifySessionDos = shopifySessionMapper.selectList(lambdaQueryWrapper);
        if (CheckTools.isNotNullOrEmpty(shopifySessionDos)) {
            // 映射为boList，这里是否抽离一层逻辑单独实现
            return converter.toBoList(shopifySessionDos);
        }
        return Collections.emptyList();
    }

}
