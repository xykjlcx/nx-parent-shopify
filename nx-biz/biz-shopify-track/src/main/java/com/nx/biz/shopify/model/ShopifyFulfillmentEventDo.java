package com.nx.biz.shopify.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("shopify_fulfillment_event")
public class ShopifyFulfillmentEventDo implements Serializable {

    @TableField(value = "shop_domain")
    private String shopDomain;

    @TableId(type = IdType.INPUT)
    @TableField(value = "id")
    private String id;
    @TableField(value = "admin_graphql_api_id")
    private String adminGraphqlApiId;
    @TableField(value = "order_id")
    private String orderId;
    @TableField(value = "fulfillment_id")
    private String fulfillmentId;
    @TableField(value = "shop_id")
    private String shopId;

    @TableField(value = "status")
    private String status;
    @TableField(value = "message")
    private String message;
    @TableField(value = "address1")
    private String address1;
    @TableField(value = "city")
    private String city;
    @TableField(value = "province")
    private String province;
    @TableField(value = "country")
    private String country;
    @TableField(value = "zip")
    private String zip;
    @TableField(value = "latitude")
    private String latitude;
    @TableField(value = "longitude")
    private String longitude;

    @TableField(value = "happened_at")
    private String happenedAt;
    @TableField(value = "created_at")
    private String createdAt;
    @TableField(value = "updated_at")
    private String updatedAt;
    @TableField(value = "estimated_delivery_at")
    private String estimatedDeliveryAt;

}
