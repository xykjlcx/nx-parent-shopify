package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class FulfillmentEventBosRoot {

    @SerializedName("fulfillment_events")
    List<FulfillmentEventBo> fulfillmentEvents;
}
