package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

public class UrlConstant {
    //oauth
    public static String OAuthRestUrl = "https://%s/admin/oauth/access_token";

    // graphql url
    public static String GraphQLUrl = "https://%s/admin/api/%s/graphql.json";

    // authCache
    public static String AuthRestUrl = "https://%s/admin/oauth/access_token";

    // fulfillment service
    public static String FulfillmentServiceRestUrl = "https://%s/admin/api/%s/fulfillment_services.json";
    public static String FulfillmentServiceInstRestUrl = "https://%s/admin/api/%s/fulfillment_services/%s.json";

    // carrier service
    public static String CarrierServiceRestUrl = "https://%s/admin/api/%s/carrier_services.json";
    public static String CarrierServiceInstRestUrl = "https://%s/admin/api/%s/carrier_services/%s.json";

    // delivery profile
    public static String DeliveryProfileRestUrl = "https://%s/admin/api/%s/graphql.json";

    // fulfillment event
    public static String FulfillmentEventRestUrl = "https://%s/admin/api/%s/orders/%s/fulfillments/%s/events.json";
    public static String FulfillmentEventInstRestUrl = "https://%s/admin/api/%s/orders/%s/fulfillments/%s/events/%s.json";

    // fulfillment order
    public static String AssignFulfillmentOrderRestUrl = "https://%s/admin/api/%s/assigned_fulfillment_orders.json";
    public static String FulfillmentOrderAcceptRestUrl = "https://%s/admin/api/%s/fulfillment_orders/{fulfillment_order_id}/fulfillment_request/accept.json";
    public static String FulfillmentOrderRejectRestUrl = "https://%s/admin/api/%s/fulfillment_orders/{fulfillment_order_id}/fulfillment_request/reject.json";
    public static String FulfillmentOrderRestUrl = "https://%s/admin/api/%s/orders/%s/fulfillment_orders.json";

    // fulfillment
    public static String FulfillmentRestUrl = "https://%s/admin/api/%s/fulfillments.json";
    public static String FulfillmentListByOrderRestUrl = "https://%s/admin/api/%s/orders/%s/fulfillments.json";
    public static String FulfillmentInstRestUrl = "https://%s/admin/api/%s/orders/%s/fulfillments/%s.json";
    public static String FulfillmentCancelRestUrl = "https://%s/admin/api/%s/fulfillments/%s/cancel.json";
    public static String FulfillmentInstUpdateRestUrl = "https://%s/admin/api/%s/fulfillments/%s/update_tracking.json";


    // location
    public static String LocationRestUrl = "https://%s/admin/api/%s/locations.json";
    public static String LocationInstRestUrl = "https://%s/admin/api/%s/locations/%s.json";
    public static String InventoryLevelRestUrl = "https://%s/admin/api/%s/locations/%s/inventory_levels.json";

    // order
    public static String OrderRestUrl = "https://%s/admin/api/%s/orders.json";
    public static String OrderInstRestUrl = "https://%s/admin/api/%s/orders/%s.json";

    // webhooks
    public static String webhookRestUrl = "https://%s/admin/api/%s/webhooks.json";
    public static String webhookInstRestUrl = "https://%s/admin/api/%s/webhooks/%s.json";
}
