package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class HualeiAuthInfo {
    @SerializedName("customer_id")
    private String customerId;
    @SerializedName("customer_userid")
    private String customerUserid;
    @SerializedName("ack")
    private String ack;
}
