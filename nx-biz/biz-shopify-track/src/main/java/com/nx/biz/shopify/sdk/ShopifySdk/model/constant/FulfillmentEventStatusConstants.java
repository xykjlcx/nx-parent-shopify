package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

public class FulfillmentEventStatusConstants {
    // The fulfillment order is ready for fulfillment.
    public static final String  open = "open";

    // The fulfillment order is being processed.
    public static final String  in_progress = "in_progress";

    // The fulfillment order is deferred and will be ready for fulfillment after the datetime specified in fulfill_at.
    public static final String  scheduled = "scheduled";

    // The fulfillment order has been cancelled by the merchant.
    public static final String  cancelled = "cancelled";

    // The fulfillment order is on hold. The fulfillment process can't be initiated until the hold on the fulfillment order is released.
    public static final String  on_hold = "on_hold";

    // The fulfillment order cannot be completed as requested.
    public static final String  incomplete = "incomplete";

    // The fulfillment order has been completed and closed.
    public static final String  closed = "closed";
}
