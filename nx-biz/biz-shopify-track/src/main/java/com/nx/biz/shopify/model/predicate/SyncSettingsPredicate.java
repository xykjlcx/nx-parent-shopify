package com.nx.biz.shopify.model.predicate;

import lombok.Data;

import java.io.Serializable;

@Data
public class SyncSettingsPredicate implements Serializable {

    private String eqId;

}
