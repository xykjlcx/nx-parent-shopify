package com.nx.biz.shopify.sdk.ShopifySdk;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentBo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentBoRoot;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentBosRoot;
import com.nx.biz.shopify.sdk.ShopifySdk.model.cmd.FulfillmentCreateReq;
import com.nx.biz.shopify.sdk.ShopifySdk.model.constant.UrlConstant;
import com.nx.biz.shopify.tools.HttpRestClientTool;
import com.nx.common.result.HttpClientResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FulfillmentSdk {

    protected static final Gson GSON = new Gson();
//    public static String urlTemplate = "https://%s.myshopify.com/admin/api/2024-04/fulfillments.json";
//    public static String urlByOrderTemplate = "https://%s.myshopify.com/admin/api/2024-04/orders/%s/fulfillments.json";
//    public static String urlInstTemplate = "https://%s.myshopify.com/admin/api/2024-04/orders/%s/fulfillments/%s.json";
//    public static String urlCancel = "https://%s.myshopify.com/admin/api/2024-04/fulfillments/%s/cancel.json";
//
//    public static String devStoreNo = "track-dev-0704";
//    public static String token = "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";

    public FulfillmentBo createFulfillment(FulfillmentCreateReq fulfillmentCreateReq, String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(urlTemplate, devStoreNo);
        String url = String.format(UrlConstant.FulfillmentRestUrl, shopDomain, apiVersion);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.post(url, GSON.toJson(fulfillmentCreateReq), "application/json", headers);
        if (clientResult.getCode() != 201) {
            log.error("createFulfillment resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String strContent = clientResult.getContent();
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(strContent).getAsJsonObject();
        jsonObject = jsonObject.get("fulfillment").getAsJsonObject();

        FulfillmentBo fulfillmentBo = GSON.fromJson(jsonObject, FulfillmentBo.class);

        return fulfillmentBo;
    }

    public FulfillmentBo cancelFulfillment(Long fulfillmentId, String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(urlCancel, devStoreNo, fulfillmentId);
        String url = String.format(UrlConstant.FulfillmentCancelRestUrl, shopDomain, apiVersion, fulfillmentId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.post(url, null, "application/json", headers);
        if (clientResult.getCode() != 200) {
            log.error("createFulfillment resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(content).getAsJsonObject();
        jsonObject = jsonObject.get("fulfillment").getAsJsonObject();

        FulfillmentBo fulfillmentBo = GSON.fromJson(jsonObject, FulfillmentBo.class);

        return fulfillmentBo;
    }

    public List<FulfillmentBo> listFulfillmentByOrderId(Long orderId,String shopDomain, String apiVersion, String accessToken) throws Exception {
        /** 1. 参数初始化 */
//        String url = String.format(urlByOrderTemplate, devStoreNo, orderId);
        String url = String.format(UrlConstant.FulfillmentListByOrderRestUrl, shopDomain, apiVersion, orderId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("queryFulfillmentOrder resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentBosRoot fulfillmentBosRoot = GSON.fromJson(content, FulfillmentBosRoot.class);
        return fulfillmentBosRoot.getFulfillments();
    }

    public FulfillmentBo getFulfillmentById(Long orderId, Long fulfillmentId,String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
//        String url = String.format(urlInstTemplate, devStoreNo, orderId, fulfillmentId);
        String url = String.format(UrlConstant.FulfillmentInstRestUrl, shopDomain, apiVersion, orderId, fulfillmentId);

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.sendGet(url, "UTF-8", headers);
        if (clientResult.getCode() != 200) {
            log.error("queryFulfillmentOrder resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String content = clientResult.getContent();
        FulfillmentBoRoot fulfillmentBoRoot = GSON.fromJson(content, FulfillmentBoRoot.class);
        return fulfillmentBoRoot.getFulfillment();
    }

    public FulfillmentBo updateTracking(FulfillmentCreateReq fulfillmentCreateReq, String shopDomain, String apiVersion, String accessToken) throws IOException {
        /** 1. 参数初始化 */
        String url = String.format(UrlConstant.FulfillmentInstUpdateRestUrl, shopDomain, apiVersion, fulfillmentCreateReq.getFulfillment().getId());

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Shopify-Access-Token", accessToken);

        /** 2. 发送请求 */
        HttpClientResult clientResult = HttpRestClientTool.post(url, GSON.toJson(fulfillmentCreateReq), "application/json", headers);
        if (clientResult.getCode() != 200) {
            log.error("createFulfillment resp: {}", clientResult);
            return null;
        }

        /** 3. 解析返回值 */
        String strContent = clientResult.getContent();
        JsonObject jsonObject = (JsonObject) new JsonParser().parse(strContent).getAsJsonObject();
        jsonObject = jsonObject.get("fulfillment").getAsJsonObject();

        FulfillmentBo fulfillmentBo = GSON.fromJson(jsonObject, FulfillmentBo.class);

        return fulfillmentBo;
    }
}
