package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;//package com.nanxun.shopifyTracking.model.bo;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//import com.google.gson.annotations.SerializedName;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import java.util.List;
//
//@NoArgsConstructor
//@Data
//public class OrderBo {
//
//    @SerializedName("app_id")
//    private Integer appId;
//    @SerializedName("billing_address")
//    private LocationBo billingAddress;
//    @SerializedName("browser_ip")
//    private String browserIp;
//    @SerializedName("buyer_accepts_marketing")
//    private Boolean buyerAcceptsMarketing;
//    @SerializedName("cancel_reason")
//    private String cancelReason;
//    @SerializedName("cancelled_at")
//    private String cancelledAt;
//    @SerializedName("cart_token")
//    private String cartToken;
//    @SerializedName("checkout_token")
//    private String checkoutToken;
//    @SerializedName("client_details")
//    private ClientDetailsDTO clientDetails;
//    @SerializedName("closed_at")
//    private String closedAt;
//    @SerializedName("company")
//    private CompanyDTO company;
//    @SerializedName("confirmation_number")
//    private String confirmationNumber;
//    @SerializedName("created_at")
//    private String createdAt;
//    @SerializedName("currency")
//    private String currency;
////    @SerializedName("current_total_additional_fees_set")
////    private CurrentTotalAdditionalFeesSetDTO currentTotalAdditionalFeesSet;
//    @SerializedName("current_total_discounts")
//    private String currentTotalDiscounts;
////    @SerializedName("current_total_discounts_set")
////    private CurrentTotalDiscountsSetDTO currentTotalDiscountsSet;
////    @SerializedName("current_total_duties_set")
////    private CurrentTotalDutiesSetDTO currentTotalDutiesSet;
//    @SerializedName("current_total_price")
//    private String currentTotalPrice;
////    @SerializedName("current_total_price_set")
////    private CurrentTotalPriceSetDTO currentTotalPriceSet;
//    @SerializedName("current_subtotal_price")
//    private String currentSubtotalPrice;
////    @SerializedName("current_subtotal_price_set")
////    private CurrentSubtotalPriceSetDTO currentSubtotalPriceSet;
//    @SerializedName("current_total_tax")
//    private String currentTotalTax;
////    @SerializedName("current_total_tax_set")
////    private CurrentTotalTaxSetDTO currentTotalTaxSet;
//    @SerializedName("customer")
//    private CustomerDTO customer;
//    @SerializedName("customer_locale")
//    private String customerLocale;
////    @SerializedName("discount_applications")
////    private List<DiscountApplicationsDTO> discountApplications;
////    @SerializedName("discount_codes")
////    private List<DiscountCodesDTO> discountCodes;
//    @SerializedName("email")
//    private String email;
//    @SerializedName("estimated_taxes")
//    private Boolean estimatedTaxes;
//    @SerializedName("financial_status")
//    private String financialStatus;
//    @SerializedName("fulfillments")
//    private List<FulfillmentsDTO> fulfillments;
//    @SerializedName("fulfillment_status")
//    private String fulfillmentStatus;
//    @SerializedName("gateway")
//    private String gateway;
//    @SerializedName("id")
//    private Integer id;
//    @SerializedName("landing_site")
//    private String landingSite;
//    @SerializedName("line_items")
//    private List<LineItemBo> lineItems;
//    @SerializedName("location_id")
//    private Integer locationId;
//    @SerializedName("merchant_of_record_app_id")
//    private Integer merchantOfRecordAppId;
//    @SerializedName("name")
//    private String name;
//    @SerializedName("note")
//    private String note;
////    @SerializedName("note_attributes")
////    private List<NoteAttributesDTO> noteAttributes;
//    @SerializedName("number")
//    private Integer number;
//    @SerializedName("order_number")
//    private Integer orderNumber;
////    @SerializedName("original_total_additional_fees_set")
////    private OriginalTotalAdditionalFeesSetDTO originalTotalAdditionalFeesSet;
////    @SerializedName("original_total_duties_set")
////    private OriginalTotalDutiesSetDTO originalTotalDutiesSet;
////    @SerializedName("payment_terms")
////    private PaymentTermsDTO paymentTerms;
//    @SerializedName("payment_gateway_names")
//    private List<String> paymentGatewayNames;
//    @SerializedName("phone")
//    private String phone;
//    @SerializedName("po_number")
//    private String poNumber;
//    @SerializedName("presentment_currency")
//    private String presentmentCurrency;
//    @SerializedName("processed_at")
//    private String processedAt;
//    @SerializedName("referring_site")
//    private String referringSite;
////    @SerializedName("refunds")
////    private List<RefundsDTO> refunds;
//    @SerializedName("shipping_address")
//    private LocationBo shippingAddress;
//    @SerializedName("shipping_lines")
//    private List<LineItemBo> shippingLines;
//    @SerializedName("source_name")
//    private String sourceName;
//    @SerializedName("source_identifier")
//    private String sourceIdentifier;
//    @SerializedName("source_url")
//    private String sourceUrl;
//    @SerializedName("subtotal_price")
//    private String subtotalPrice;
////    @SerializedName("subtotal_price_set")
////    private SubtotalPriceSetDTO subtotalPriceSet;
//    @SerializedName("tags")
//    private String tags;
//    @SerializedName("tax_lines")
//    private List<TaxLinesDTO> taxLines;
//    @SerializedName("taxes_included")
//    private Boolean taxesIncluded;
//    @SerializedName("test")
//    private Boolean test;
//    @SerializedName("token")
//    private String token;
//    @SerializedName("total_discounts")
//    private String totalDiscounts;
////    @SerializedName("total_discounts_set")
////    private TotalDiscountsSetDTO totalDiscountsSet;
////    @SerializedName("total_line_items_price")
////    private String totalLineItemsPrice;
////    @SerializedName("total_line_items_price_set")
////    private TotalLineItemsPriceSetDTO totalLineItemsPriceSet;
//    @SerializedName("total_outstanding")
//    private String totalOutstanding;
//    @SerializedName("total_price")
//    private String totalPrice;
////    @SerializedName("total_price_set")
////    private TotalPriceSetDTO totalPriceSet;
////    @SerializedName("total_shipping_price_set")
////    private TotalShippingPriceSetDTO totalShippingPriceSet;
//    @SerializedName("total_tax")
//    private String totalTax;
////    @SerializedName("total_tax_set")
////    private TotalTaxSetDTO totalTaxSet;
//    @SerializedName("total_tip_received")
//    private String totalTipReceived;
//    @SerializedName("total_weight")
//    private Integer totalWeight;
//    @SerializedName("updated_at")
//    private String updatedAt;
//    @SerializedName("user_id")
//    private Integer userId;
////    @SerializedName("order_status_url")
////    private OrderStatusUrlDTO orderStatusUrl;
//
//    @NoArgsConstructor
//    @Data
//    public static class BillingAddressDTO {
//        @SerializedName("address1")
//        private String address1;
//        @SerializedName("address2")
//        private String address2;
//        @SerializedName("city")
//        private String city;
//        @SerializedName("company")
//        private Object company;
//        @SerializedName("country")
//        private String country;
//        @SerializedName("first_name")
//        private String firstName;
//        @SerializedName("last_name")
//        private String lastName;
//        @SerializedName("phone")
//        private String phone;
//        @SerializedName("province")
//        private String province;
//        @SerializedName("zip")
//        private String zip;
//        @SerializedName("name")
//        private String name;
//        @SerializedName("province_code")
//        private String provinceCode;
//        @SerializedName("country_code")
//        private String countryCode;
//        @SerializedName("latitude")
//        private String latitude;
//        @SerializedName("longitude")
//        private String longitude;
//    }
//
//    @NoArgsConstructor
//    @Data
//    public static class ClientDetailsDTO {
//        @SerializedName("accept_language")
//        private String acceptLanguage;
//        @SerializedName("browser_height")
//        private Integer browserHeight;
//        @SerializedName("browser_ip")
//        private String browserIp;
//        @SerializedName("browser_width")
//        private Integer browserWidth;
//        @SerializedName("session_hash")
//        private String sessionHash;
//        @SerializedName("user_agent")
//        private String userAgent;
//    }
//
//    @NoArgsConstructor
//    @Data
//    public static class CompanyDTO {
//        @SerializedName("id")
//        private Integer id;
//        @SerializedName("location_id")
//        private Integer locationId;
//    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentTotalAdditionalFeesSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentTotalDiscountsSetDTO {
////        @SerializedName("current_total_discounts_set")
////        private CurrentTotalDiscountsSetDTO.CurrentTotalDiscountsSetDTO currentTotalDiscountsSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class CurrentTotalDiscountsSetDTO {
////            @SerializedName("shop_money")
////            private CurrentTotalDiscountsSetDTO.CurrentTotalDiscountsSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private CurrentTotalDiscountsSetDTO.CurrentTotalDiscountsSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentTotalDutiesSetDTO {
////        @SerializedName("current_total_duties_set")
////        private CurrentTotalDutiesSetDTO.CurrentTotalDutiesSetDTO currentTotalDutiesSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class CurrentTotalDutiesSetDTO {
////            @SerializedName("shop_money")
////            private CurrentTotalDutiesSetDTO.CurrentTotalDutiesSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private CurrentTotalDutiesSetDTO.CurrentTotalDutiesSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentTotalPriceSetDTO {
////        @SerializedName("current_total_price_set")
////        private CurrentTotalPriceSetDTO.CurrentTotalPriceSetDTO currentTotalPriceSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class CurrentTotalPriceSetDTO {
////            @SerializedName("shop_money")
////            private CurrentTotalPriceSetDTO.CurrentTotalPriceSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private CurrentTotalPriceSetDTO.CurrentTotalPriceSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentSubtotalPriceSetDTO {
////        @SerializedName("current_subtotal_price_set")
////        private CurrentSubtotalPriceSetDTO.CurrentSubtotalPriceSetDTO currentSubtotalPriceSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class CurrentSubtotalPriceSetDTO {
////            @SerializedName("shop_money")
////            private CurrentSubtotalPriceSetDTO.CurrentSubtotalPriceSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private CurrentSubtotalPriceSetDTO.CurrentSubtotalPriceSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class CurrentTotalTaxSetDTO {
////        @SerializedName("current_total_tax_set")
////        private CurrentTotalTaxSetDTO.CurrentTotalTaxSetDTO currentTotalTaxSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class CurrentTotalTaxSetDTO {
////            @SerializedName("shop_money")
////            private CurrentTotalTaxSetDTO.CurrentTotalTaxSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private CurrentTotalTaxSetDTO.CurrentTotalTaxSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
//
//    @NoArgsConstructor
//    @Data
//    public static class CustomerDTO {
//        @SerializedName("id")
//        private Integer id;
//        @SerializedName("email")
//        private String email;
//        @SerializedName("accepts_marketing")
//        private Boolean acceptsMarketing;
//        @SerializedName("created_at")
//        private String createdAt;
//        @SerializedName("updated_at")
//        private String updatedAt;
//        @SerializedName("first_name")
//        private String firstName;
//        @SerializedName("last_name")
//        private String lastName;
//        @SerializedName("state")
//        private String state;
//        @SerializedName("note")
//        private Object note;
//        @SerializedName("verified_email")
//        private Boolean verifiedEmail;
//        @SerializedName("multipass_identifier")
//        private Object multipassIdentifier;
//        @SerializedName("tax_exempt")
//        private Boolean taxExempt;
//        @SerializedName("tax_exemptions")
//        private TaxExemptionsDTO taxExemptions;
//        @SerializedName("phone")
//        private String phone;
//        @SerializedName("tags")
//        private String tags;
//        @SerializedName("currency")
//        private String currency;
//        @SerializedName("addresses")
//        private AddressesDTO addresses;
//        @SerializedName("admin_graphql_api_id")
//        private String adminGraphqlApiId;
//        @SerializedName("default_address")
//        private DefaultAddressDTO defaultAddress;
//
//        @NoArgsConstructor
//        @Data
//        public static class TaxExemptionsDTO {
//        }
//
//        @NoArgsConstructor
//        @Data
//        public static class AddressesDTO {
//        }
//
//        @NoArgsConstructor
//        @Data
//        public static class DefaultAddressDTO {
//        }
//    }
//
////    @NoArgsConstructor
////    @Data
////    public static class OriginalTotalAdditionalFeesSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class OriginalTotalDutiesSetDTO {
////        @SerializedName("original_total_duties_set")
////        private OriginalTotalDutiesSetDTO.OriginalTotalDutiesSetDTO originalTotalDutiesSet;
////
////        @NoArgsConstructor
////        @Data
////        public static class OriginalTotalDutiesSetDTO {
////            @SerializedName("shop_money")
////            private OriginalTotalDutiesSetDTO.OriginalTotalDutiesSetDTO.ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private OriginalTotalDutiesSetDTO.OriginalTotalDutiesSetDTO.PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class PaymentTermsDTO {
////        @SerializedName("amount")
////        private Integer amount;
////        @SerializedName("currency")
////        private String currency;
////        @SerializedName("payment_terms_name")
////        private String paymentTermsName;
////        @SerializedName("payment_terms_type")
////        private String paymentTermsType;
////        @SerializedName("due_in_days")
////        private Integer dueInDays;
////        @SerializedName("payment_schedules")
////        private List<PaymentSchedulesDTO> paymentSchedules;
////
////        @NoArgsConstructor
////        @Data
////        public static class PaymentSchedulesDTO {
////            @SerializedName("amount")
////            private Integer amount;
////            @SerializedName("currency")
////            private String currency;
////            @SerializedName("issued_at")
////            private String issuedAt;
////            @SerializedName("due_at")
////            private String dueAt;
////            @SerializedName("completed_at")
////            private String completedAt;
////            @SerializedName("expected_payment_method")
////            private String expectedPaymentMethod;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class ShippingAddressDTO {
////        @SerializedName("address1")
////        private String address1;
////        @SerializedName("address2")
////        private String address2;
////        @SerializedName("city")
////        private String city;
////        @SerializedName("company")
////        private Object company;
////        @SerializedName("country")
////        private String country;
////        @SerializedName("first_name")
////        private String firstName;
////        @SerializedName("last_name")
////        private String lastName;
////        @SerializedName("latitude")
////        private String latitude;
////        @SerializedName("longitude")
////        private String longitude;
////        @SerializedName("phone")
////        private String phone;
////        @SerializedName("province")
////        private String province;
////        @SerializedName("zip")
////        private String zip;
////        @SerializedName("name")
////        private String name;
////        @SerializedName("country_code")
////        private String countryCode;
////        @SerializedName("province_code")
////        private String provinceCode;
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class SubtotalPriceSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class TotalDiscountsSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class TotalLineItemsPriceSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class TotalPriceSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class TotalShippingPriceSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class TotalTaxSetDTO {
////        @SerializedName("shop_money")
////        private ShopMoneyDTO shopMoney;
////        @SerializedName("presentment_money")
////        private PresentmentMoneyDTO presentmentMoney;
////
////        @NoArgsConstructor
////        @Data
////        public static class ShopMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PresentmentMoneyDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("currency_code")
////            private String currencyCode;
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class OrderStatusUrlDTO {
////        @SerializedName("order_status_url")
////        private String orderStatusUrl;
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class DiscountApplicationsDTO {
////        @SerializedName("type")
////        private String type;
////        @SerializedName("title")
////        private String title;
////        @SerializedName("description")
////        private String description;
////        @SerializedName("value")
////        private String value;
////        @SerializedName("value_type")
////        private String valueType;
////        @SerializedName("allocation_method")
////        private String allocationMethod;
////        @SerializedName("target_selection")
////        private String targetSelection;
////        @SerializedName("target_type")
////        private String targetType;
////        @SerializedName("code")
////        private String code;
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class DiscountCodesDTO {
////        @SerializedName("code")
////        private String code;
////        @SerializedName("amount")
////        private String amount;
////        @SerializedName("type")
////        private String type;
////    }
//
//    @NoArgsConstructor
//    @Data
//    public static class FulfillmentsDTO {
//        @SerializedName("created_at")
//        private String createdAt;
//        @SerializedName("id")
//        private Integer id;
//        @SerializedName("order_id")
//        private Integer orderId;
//        @SerializedName("status")
//        private String status;
//        @SerializedName("tracking_company")
//        private String trackingCompany;
//        @SerializedName("tracking_number")
//        private String trackingNumber;
//        @SerializedName("updated_at")
//        private String updatedAt;
//    }
//
////    @NoArgsConstructor
////    @Data
////    public static class LineItemsDTO {
////        @SerializedName("attributed_staffs")
////        private List<AttributedStaffsDTO> attributedStaffs;
////        @SerializedName("fulfillable_quantity")
////        private Integer fulfillableQuantity;
////        @SerializedName("fulfillment_service")
////        private String fulfillmentService;
////        @SerializedName("fulfillment_status")
////        private String fulfillmentStatus;
////        @SerializedName("grams")
////        private Integer grams;
////        @SerializedName("id")
////        private Integer id;
////        @SerializedName("price")
////        private String price;
////        @SerializedName("product_id")
////        private Integer productId;
////        @SerializedName("quantity")
////        private Integer quantity;
////        @SerializedName("current_quantity")
////        private Integer currentQuantity;
////        @SerializedName("requires_shipping")
////        private Boolean requiresShipping;
////        @SerializedName("sku")
////        private String sku;
////        @SerializedName("title")
////        private String title;
////        @SerializedName("variant_id")
////        private Integer variantId;
////        @SerializedName("variant_title")
////        private String variantTitle;
////        @SerializedName("vendor")
////        private String vendor;
////        @SerializedName("name")
////        private String name;
////        @SerializedName("gift_card")
////        private Boolean giftCard;
////        @SerializedName("price_set")
////        private PriceSetDTO priceSet;
////        @SerializedName("properties")
////        private List<PropertiesDTO> properties;
////        @SerializedName("taxable")
////        private Boolean taxable;
////        @SerializedName("tax_lines")
////        private List<TaxLinesDTO> taxLines;
////        @SerializedName("total_discount")
////        private String totalDiscount;
////        @SerializedName("total_discount_set")
////        private TotalDiscountSetDTO totalDiscountSet;
////        @SerializedName("discount_allocations")
////        private List<DiscountAllocationsDTO> discountAllocations;
////        @SerializedName("origin_location")
////        private OriginLocationDTO originLocation;
////        @SerializedName("duties")
////        private List<DutiesDTO> duties;
////
////        @NoArgsConstructor
////        @Data
////        public static class PriceSetDTO {
////            @SerializedName("shop_money")
////            private ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class TotalDiscountSetDTO {
////            @SerializedName("shop_money")
////            private ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class OriginLocationDTO {
////            @SerializedName("id")
////            private Long id;
////            @SerializedName("country_code")
////            private String countryCode;
////            @SerializedName("province_code")
////            private String provinceCode;
////            @SerializedName("name")
////            private String name;
////            @SerializedName("address1")
////            private String address1;
////            @SerializedName("address2")
////            private String address2;
////            @SerializedName("city")
////            private String city;
////            @SerializedName("zip")
////            private String zip;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class AttributedStaffsDTO {
////            @SerializedName("id")
////            private String id;
////            @SerializedName("quantity")
////            private Integer quantity;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class PropertiesDTO {
////            @SerializedName("name")
////            private String name;
////            @SerializedName("value")
////            private String value;
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class TaxLinesDTO {
////            @SerializedName("title")
////            private String title;
////            @SerializedName("price")
////            private String price;
////            @SerializedName("price_set")
////            private PriceSetDTO priceSet;
////            @SerializedName("channel_liable")
////            private Boolean channelLiable;
////            @SerializedName("rate")
////            private Double rate;
////
////            @NoArgsConstructor
////            @Data
////            public static class PriceSetDTO {
////                @SerializedName("shop_money")
////                private ShopMoneyDTO shopMoney;
////                @SerializedName("presentment_money")
////                private PresentmentMoneyDTO presentmentMoney;
////
////                @NoArgsConstructor
////                @Data
////                public static class ShopMoneyDTO {
////                    @SerializedName("amount")
////                    private String amount;
////                    @SerializedName("currency_code")
////                    private String currencyCode;
////                }
////
////                @NoArgsConstructor
////                @Data
////                public static class PresentmentMoneyDTO {
////                    @SerializedName("amount")
////                    private String amount;
////                    @SerializedName("currency_code")
////                    private String currencyCode;
////                }
////            }
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class DiscountAllocationsDTO {
////            @SerializedName("amount")
////            private String amount;
////            @SerializedName("discount_application_index")
////            private Integer discountApplicationIndex;
////            @SerializedName("amount_set")
////            private AmountSetDTO amountSet;
////
////            @NoArgsConstructor
////            @Data
////            public static class AmountSetDTO {
////                @SerializedName("shop_money")
////                private ShopMoneyDTO shopMoney;
////                @SerializedName("presentment_money")
////                private PresentmentMoneyDTO presentmentMoney;
////
////                @NoArgsConstructor
////                @Data
////                public static class ShopMoneyDTO {
////                    @SerializedName("amount")
////                    private String amount;
////                    @SerializedName("currency_code")
////                    private String currencyCode;
////                }
////
////                @NoArgsConstructor
////                @Data
////                public static class PresentmentMoneyDTO {
////                    @SerializedName("amount")
////                    private String amount;
////                    @SerializedName("currency_code")
////                    private String currencyCode;
////                }
////            }
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class DutiesDTO {
////            @SerializedName("id")
////            private String id;
////            @SerializedName("harmonized_system_code")
////            private String harmonizedSystemCode;
////            @SerializedName("country_code_of_origin")
////            private String countryCodeOfOrigin;
////            @SerializedName("shop_money")
////            private ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private PresentmentMoneyDTO presentmentMoney;
////            @SerializedName("tax_lines")
////            private List<TaxLinesDTO> taxLines;
////            @SerializedName("admin_graphql_api_id")
////            private String adminGraphqlApiId;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class TaxLinesDTO {
////                @SerializedName("title")
////                private String title;
////                @SerializedName("price")
////                private String price;
////                @SerializedName("rate")
////                private Double rate;
////                @SerializedName("price_set")
////                private PriceSetDTO priceSet;
////                @SerializedName("channel_liable")
////                private Boolean channelLiable;
////
////                @NoArgsConstructor
////                @Data
////                public static class PriceSetDTO {
////                    @SerializedName("shop_money")
////                    private ShopMoneyDTO shopMoney;
////                    @SerializedName("presentment_money")
////                    private PresentmentMoneyDTO presentmentMoney;
////
////                    @NoArgsConstructor
////                    @Data
////                    public static class ShopMoneyDTO {
////                        @SerializedName("amount")
////                        private String amount;
////                        @SerializedName("currency_code")
////                        private String currencyCode;
////                    }
////
////                    @NoArgsConstructor
////                    @Data
////                    public static class PresentmentMoneyDTO {
////                        @SerializedName("amount")
////                        private String amount;
////                        @SerializedName("currency_code")
////                        private String currencyCode;
////                    }
////                }
////            }
////        }
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class NoteAttributesDTO {
////        @SerializedName("name")
////        private String name;
////        @SerializedName("value")
////        private String value;
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class RefundsDTO {
////        @SerializedName("id")
////        private Long id;
////        @SerializedName("order_id")
////        private Long orderId;
////        @SerializedName("created_at")
////        private String createdAt;
////        @SerializedName("note")
////        private Object note;
////        @SerializedName("user_id")
////        private Object userId;
////        @SerializedName("processed_at")
////        private String processedAt;
////        @SerializedName("refund_line_items")
////        private List<?> refundLineItems;
////        @SerializedName("transactions")
////        private List<?> transactions;
////        @SerializedName("order_adjustments")
////        private List<?> orderAdjustments;
////    }
////
////    @NoArgsConstructor
////    @Data
////    public static class ShippingLinesDTO {
////        @SerializedName("code")
////        private String code;
////        @SerializedName("price")
////        private String price;
////        @SerializedName("price_set")
////        private PriceSetDTO priceSet;
////        @SerializedName("discounted_price")
////        private String discountedPrice;
////        @SerializedName("discounted_price_set")
////        private DiscountedPriceSetDTO discountedPriceSet;
////        @SerializedName("source")
////        private String source;
////        @SerializedName("title")
////        private String title;
////        @SerializedName("tax_lines")
////        private List<?> taxLines;
////        @SerializedName("carrier_identifier")
////        private String carrierIdentifier;
////        @SerializedName("requested_fulfillment_service_id")
////        private String requestedFulfillmentServiceId;
////        @SerializedName("is_removed")
////        private Boolean isRemoved;
////
////        @NoArgsConstructor
////        @Data
////        public static class PriceSetDTO {
////            @SerializedName("shop_money")
////            private ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////
////        @NoArgsConstructor
////        @Data
////        public static class DiscountedPriceSetDTO {
////            @SerializedName("shop_money")
////            private ShopMoneyDTO shopMoney;
////            @SerializedName("presentment_money")
////            private PresentmentMoneyDTO presentmentMoney;
////
////            @NoArgsConstructor
////            @Data
////            public static class ShopMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////
////            @NoArgsConstructor
////            @Data
////            public static class PresentmentMoneyDTO {
////                @SerializedName("amount")
////                private String amount;
////                @SerializedName("currency_code")
////                private String currencyCode;
////            }
////        }
////    }
//
//    @NoArgsConstructor
//    @Data
//    public static class TaxLinesDTO {
//        @SerializedName("price")
//        private Double price;
//        @SerializedName("rate")
//        private Double rate;
//        @SerializedName("title")
//        private String title;
//        @SerializedName("channel_liable")
//        private Boolean channelLiable;
//    }
//}
