package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class FulfillmentEventBo {

    @SerializedName("id")
    private Long id;
    @SerializedName("fulfillment_id")
    private Long fulfillmentId;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("happened_at")
    private String happenedAt;
    @SerializedName("city")
    private String city;
    @SerializedName("province")
    private String province;
    @SerializedName("country")
    private String country;
    @SerializedName("zip")
    private String zip;
    @SerializedName("address1")
    private String address1;
    @SerializedName("latitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("shop_id")
    private Long shopId;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("estimated_delivery_at")
    private String estimatedDeliveryAt;
    @SerializedName("order_id")
    private Long orderId;
    @SerializedName("admin_graphql_api_id")
    private String adminGraphqlApiId;
}
