package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

/**
 * https://shopify.dev/docs/api/admin-rest/2024-10/resources/order#get-orders?status=any
     open: Show only open orders.
     closed: Show only closed orders.
     cancelled: Show only canceled orders.
     any: Show orders of any status, including archived orders.
 */
public class OrderStatus {
    public static final String open = "open";
    public static final String closed = "closed";
    public static final String cancelled = "cancelled";
    public static final String any = "any";
}
