package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import cn.hutool.core.lang.Assert;
import com.nx.common.tools.CheckTools;
import lombok.Data;

@Data
public class LabelPrintedReq {
    String orderId;
    String company;
    String trackNumber;
    String transferNo;
    String trackUrl;

    public void check() {
        Assert.isTrue(CheckTools.isNotNullOrEmpty(orderId), "订单id为空");
        Assert.isTrue(CheckTools.isNotNullOrEmpty(trackNumber), "trackNumber为空");
    }
}
