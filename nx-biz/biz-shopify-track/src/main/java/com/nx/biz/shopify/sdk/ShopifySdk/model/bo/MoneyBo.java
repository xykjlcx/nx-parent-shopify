package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class MoneyBo {
    private String amount;
    private String currency_code;
}
