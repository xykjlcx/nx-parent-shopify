package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class FulfillmentEventCreateReq {
    @SerializedName("id")
    private Long id;
//    @SerializedName("order_id")
//    private Long orderId;
//    @SerializedName("fulfillment_id")
//    private Long fulfillmentId;
    @SerializedName("shop_id")
    private Long shopId;

    @SerializedName("address1")
    private String address1;
    @SerializedName("zip")
    private String zip;
    @SerializedName("city")
    private String city;
    @SerializedName("province")
    private String province;
    @SerializedName("country")
    private String country;
    @SerializedName("latitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    @SerializedName("estimated_delivery_at")
    private String estimatedDeliveryAt;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("happened_at")
    private String happenedAt;

//    String fulfillmentId;
//    String status;
//    String happenedAt;
//    String latitude;
//    String longitude;
//    String message;
//    String city;
//    String province;
//    String country;
//    String zip;
//    String address1;

}
