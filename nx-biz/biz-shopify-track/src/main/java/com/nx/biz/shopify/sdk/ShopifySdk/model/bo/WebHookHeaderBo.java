package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class WebHookHeaderBo {
    String topic;
    String hmacSha256;
    String shopDomain;
    String webHookId;
    String triggeredAt;
    String eventId;
}
