package com.nx.biz.shopify.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sync_settings")
public class SyncSettingsDo {

    @TableId(type = IdType.INPUT)
    @TableField(value = "id")
    private String id;

    @TableField(value = "track_company")
    private String trackCompany;

    @TableField(value = "track_company_url")
    private String trackCompanyUrl;

}
