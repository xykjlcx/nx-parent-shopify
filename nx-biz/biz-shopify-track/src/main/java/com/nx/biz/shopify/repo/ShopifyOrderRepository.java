package com.nx.biz.shopify.repo;

import com.nx.biz.shopify.api.bo.ShopifyOrder;
import com.nx.biz.shopify.model.predicate.ShopifyOrderPredicate;
import com.nx.common.base.BaseRepository;

public interface ShopifyOrderRepository extends BaseRepository<ShopifyOrder, String, ShopifyOrderPredicate> {

}
