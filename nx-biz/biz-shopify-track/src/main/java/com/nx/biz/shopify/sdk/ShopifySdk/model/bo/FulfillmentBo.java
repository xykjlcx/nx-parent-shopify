package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

import java.util.List;

@Data
public class FulfillmentBo {
    String id;
    String name;
    String admin_graphql_api_id;

    Long location_id;
    Long order_id;
    String email;
    LocationDetailBo destination;
    LocationBo origin_address;
    List<LineItemBo> line_items;
    // 收据
    ReceiptBo receipt;
    String service;
    String shipment_status;
    String status;
    String tracking_company;

    String tracking_number;
    List<String> tracking_numbers;
    String tracking_url;
    List<String> tracking_urls;
//    String variant_inventory_management;
    String updated_at;
    String created_at;

}
