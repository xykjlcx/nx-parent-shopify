package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

import java.util.List;

@Data
public class DutyBo {
    private String id;
    private String harmonized_system_code;
    private String country_code_of_origin;
    private List<MoneyBo> shop_money;
    private MoneyBo presentment_money;
    private List<TaxLineBo> tax_lines;
    private String admin_graphql_api_id;
}
