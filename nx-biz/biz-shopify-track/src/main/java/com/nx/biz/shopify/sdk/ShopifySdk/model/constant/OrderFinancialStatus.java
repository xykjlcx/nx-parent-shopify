package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

/**
 pending: The payments are pending. Payment might fail in this state. Check again to confirm whether the payments have been paid successfully.
 authorized: The payments have been authorized.
 partially_paid: The order has been partially paid.
 paid: The payments have been paid.
 partially_refunded: The payments have been partially refunded.
 refunded: The payments have been refunded.
 voided: The payments have been voided.
 */
public class OrderFinancialStatus {
    public static final String pending = "pending";
    public static final String authorized = "authorized";
    public static final String partially_paid = "partially_paid";
    public static final String paid = "paid";
    public static final String partially_refunded = "partially_refunded";
    public static final String refunded = "refunded";
    public static final String voided = "voided";
}
