package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class HualeiProduct {

    @SerializedName("_key")
    private String key;
    @SerializedName("express_type")
    private String expressType;
    @SerializedName("print_minor_languages")
    private String printMinorLanguages;
    @SerializedName("product_doornorule")
    private String productDoornorule;
    @SerializedName("product_id")
    private String productId;
    @SerializedName("product_note")
    private String productNote;
    @SerializedName("product_shortname")
    private String productShortname;
    @SerializedName("product_shortnumber")
    private String productShortnumber;
    @SerializedName("product_tracknoapitype")
    private String productTracknoapitype;
}
