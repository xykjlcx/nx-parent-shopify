package com.nx.biz.shopify.repo.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nx.biz.shopify.api.bo.ShopifyFulfillmentEvent;
import com.nx.biz.shopify.convert.ShopifyFulfillmentEventConverter;
import com.nx.biz.shopify.mapper.ShopifyFulfillmentEventMapper;
import com.nx.biz.shopify.model.ShopifyFulfillmentEventDo;
import com.nx.biz.shopify.model.predicate.ShopifyFulfillmentEventPredicate;
import com.nx.biz.shopify.repo.ShopifyFulfillmentEventRepository;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Slf4j
@Repository
public class ShopifyFulfillmentEventRepoImpl implements ShopifyFulfillmentEventRepository {

    @Resource
    private ShopifyFulfillmentEventMapper shopifyFulfillmentEventMapper;
    @Resource
    private ShopifyFulfillmentEventConverter shopifyFulfillmentEventConverter;

    // 将抽象的查询条件，转化为mybatis plus的检索参数
    private LambdaQueryWrapper<ShopifyFulfillmentEventDo> buildQueryWrapper(ShopifyFulfillmentEventPredicate predicate) {
        LambdaQueryWrapper<ShopifyFulfillmentEventDo> queryWrapper = new LambdaQueryWrapper<>();
        if (CheckTools.isNullOrEmpty(predicate)) {
            return queryWrapper;
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqId())) {
            queryWrapper.eq(ShopifyFulfillmentEventDo::getId, predicate.getEqId());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqFulfillmentId())) {
            queryWrapper.eq(ShopifyFulfillmentEventDo::getFulfillmentId, predicate.getEqFulfillmentId());
        }
        return queryWrapper;
    }

    @Override
    public String save(ShopifyFulfillmentEvent shopifyFulfillmentEventBo) {
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentEventBo)) {
            throw new RuntimeException("shopifyFulfillmentEventBo is null");
        }
        ShopifyFulfillmentEventDo fulfillmentEventConverterDo = shopifyFulfillmentEventConverter.toDo(shopifyFulfillmentEventBo);
        if (CheckTools.isNullOrEmpty(fulfillmentEventConverterDo.getId())) {
            // 如果id业务没有设置，则生成一个
            fulfillmentEventConverterDo.setId(IdUtil.simpleUUID());
        }
        // 调用mybatis落库
        shopifyFulfillmentEventMapper.insert(fulfillmentEventConverterDo);
        return fulfillmentEventConverterDo.getId();
    }

    @Override
    public void update(ShopifyFulfillmentEvent shopifyFulfillmentEventBo) {
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentEventBo)) {
            throw new RuntimeException("shopifyFulfillmentEventBo is null");
        }
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentEventBo.getId())) {
            throw new RuntimeException("shopifyFulfillmentEventBo.id is null, can not update shopifyFulfillmentEventBo without id");
        }
        // 由于是业务对象映射到数据对象，所以这一步一定要保证没有信息的丢失
        ShopifyFulfillmentEventDo fulfillmentEventConverterDo = shopifyFulfillmentEventConverter.toDo(shopifyFulfillmentEventBo);
        // 调用mybatis落库
        shopifyFulfillmentEventMapper.updateById(fulfillmentEventConverterDo);
    }

    @Override
    public void deleteById(String dbId) {
        shopifyFulfillmentEventMapper.deleteById(dbId);
    }

    @Override
    public ShopifyFulfillmentEvent queryById(String dbId) {
        ShopifyFulfillmentEventDo shopifyFulfillmentEventDo = shopifyFulfillmentEventMapper.selectById(dbId);
        if (CheckTools.isNotNullOrEmpty(shopifyFulfillmentEventDo)) {
            ShopifyFulfillmentEvent shopifyFulfillmentEventBo = shopifyFulfillmentEventConverter.toBo(shopifyFulfillmentEventDo);
            return shopifyFulfillmentEventBo;
        }
        return null;
    }

    @Override
    public List<ShopifyFulfillmentEvent> queryList(ShopifyFulfillmentEventPredicate predicate) {
        LambdaQueryWrapper<ShopifyFulfillmentEventDo> lambdaQueryWrapper = this.buildQueryWrapper(predicate);
        List<ShopifyFulfillmentEventDo> shopifyFulfillmentEventDos = shopifyFulfillmentEventMapper.selectList(lambdaQueryWrapper);
        if (CheckTools.isNotNullOrEmpty(shopifyFulfillmentEventDos)) {
            // 映射为boList，这里是否抽离一层逻辑单独实现
            return shopifyFulfillmentEventConverter.toBoList(shopifyFulfillmentEventDos);
        }
        return Collections.emptyList();
    }

}
