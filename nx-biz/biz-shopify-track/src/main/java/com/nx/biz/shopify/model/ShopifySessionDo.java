package com.nx.biz.shopify.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("shopify_sessions")
public class ShopifySessionDo implements Serializable {

    @TableId(type = IdType.INPUT)
    @TableField(value = "id")
    private String id;

    @TableField(value = "shop")
    private String shop;

    @TableField(value = "state")
    private String state;

    @TableField(value = "isOnline")
    private Boolean isOnline;

    @TableField(value = "scope")
    private String scope;

    @TableField(value = "expires")
    private Integer expires;

    @TableField(value = "onlineAccessInfo")
    private String onlineAccessInfo;

    @TableField(value = "accessToken")
    private String accessToken;

}
