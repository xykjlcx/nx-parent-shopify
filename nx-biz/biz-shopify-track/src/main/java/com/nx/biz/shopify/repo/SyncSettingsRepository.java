package com.nx.biz.shopify.repo;

import com.nx.biz.shopify.api.bo.SyncSettings;
import com.nx.biz.shopify.model.predicate.SyncSettingsPredicate;
import com.nx.common.base.BaseRepository;

public interface SyncSettingsRepository extends BaseRepository<SyncSettings, String, SyncSettingsPredicate> {

}
