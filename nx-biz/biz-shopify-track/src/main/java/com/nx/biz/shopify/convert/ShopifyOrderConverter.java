package com.nx.biz.shopify.convert;

import com.nx.biz.shopify.api.bo.ShopifyOrder;
import com.nx.biz.shopify.model.ShopifyOrderDo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.OrderBoV2;
import com.nx.common.tools.CheckTools;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShopifyOrderConverter {

    public ShopifyOrder toBo(ShopifyOrderDo doItem) {
        ShopifyOrder boItem = new ShopifyOrder();
        BeanUtils.copyProperties(doItem, boItem);
        return boItem;
    }

    public ShopifyOrderDo toDo(ShopifyOrder boItem) {
        ShopifyOrderDo doItem = new ShopifyOrderDo();
        BeanUtils.copyProperties(boItem, doItem);
        return doItem;
    }

    public List<ShopifyOrder> toBoList(List<ShopifyOrderDo> doList) {
        List<ShopifyOrder> boList = new ArrayList<>();
        for (ShopifyOrderDo doItem : doList) {
            ShopifyOrder boItem = this.toBo(doItem);
            // 加入list
            boList.add(boItem);
        }
        return boList;
    }

    public List<ShopifyOrderDo> toDoList(List<ShopifyOrder> boList) {
        List<ShopifyOrderDo> doList = new ArrayList<>();
        for (ShopifyOrder boItem : boList) {
            ShopifyOrderDo doItem = this.toDo(boItem);
            // 加入list
            doList.add(doItem);
        }
        return doList;
    }

    /* ------------------------------------------------------------------------------------ */

    public ShopifyOrder orderBo2OrderEo(ShopifyOrder shopifyOrder, OrderBoV2 orderBo) {
        ShopifyOrder shopifyOrderBo = new ShopifyOrder();

        BeanUtils.copyProperties(orderBo, shopifyOrderBo);
        shopifyOrderBo.setId(String.valueOf(orderBo.getId()));

        if (CheckTools.isNotNullOrEmpty(shopifyOrder)) {
            shopifyOrderBo.setTrackNumber(shopifyOrder.getTrackNumber());
        }
        return shopifyOrderBo;

//        orderEo.setAdminGraphqlApiId(orderBo.getAdminGraphqlApiId());
//        orderEo.setName(orderBo.getName());
//        orderEo.setFinancialStatus(orderBo.getFinancialStatus());
//        orderEo.setFulfillmentStatus(orderBo.getFulfillmentStatus());
//        orderEo.setTotalPrice(orderBo.getTotalPrice());
//        orderEo.setTotalLineItemsPrice(orderBo.getTotalLineItemsPrice());
    }

}
