package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

import org.springframework.stereotype.Component;

@Component
public class TopicConstants {
    public static final String orderCreate = "orders/create";
    public static final String orderUpdate = "orders/updated";
    public static final String orderDelete = "orders/delete";
    public static final String orderFulfilled = "orders/fulfilled";
    public static final String orderPaid = "orders/paid";

    //
    public static final String fulfillmentCreate = "fulfillments/create";

    // Occurs when a fulfillment service accepts a merchant's request to fulfill one or more fulfillment order line items
    public static final String fulfillmentRequestCreate = "fulfillment_orders/fulfillment_request_accepted";

}
