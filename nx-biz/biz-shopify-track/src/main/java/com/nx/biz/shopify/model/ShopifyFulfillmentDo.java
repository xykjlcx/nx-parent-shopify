package com.nx.biz.shopify.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("shopify_fulfillment")
public class ShopifyFulfillmentDo implements Serializable {

    @TableField(value = "shop_domain")
    private String shopDomain;

    @TableId(type = IdType.INPUT)
    @TableField(value = "id")
    private String id;

    @TableField(value = "admin_graphql_api_id")
    private String adminGraphqlApiId;

    @TableField(value = "name")
    private String name;

    @TableField(value = "shop_id")
    private String shopId;

    @TableField(value = "order_id")
    private String orderId;

    @TableField(value = "fulfillment_order_id")
    private String fulfillmentOrderId;

    @TableField(value = "status")
    private String status;

    @TableField(value = "shipment_status")
    private String shipmentStatus;

    @TableField(value = "service")
    private String service;

    @TableField(value = "location_id")
    private String locationId;

    @TableField(value = "email")
    private String email;

    @TableField(value = "tracking_company")
    private String trackingCompany;

    @TableField(value = "tracking_number")
    private String trackingNumber;

    @TableField(value = "tracking_url")
    private String trackingUrl;

    @TableField(value = "destination")
    private String destination;

    @TableField(value = "items")
    private String items;

    @TableField(value = "create_at")
    private String createAt;

    @TableField(value = "update_at")
    private String updateAt;

    @TableField(value = "transfer_no")
    private String transferNo;

    // 0未同步 1已同步
    @TableField(value = "sync_flag")
    private Integer syncFlag;

}
