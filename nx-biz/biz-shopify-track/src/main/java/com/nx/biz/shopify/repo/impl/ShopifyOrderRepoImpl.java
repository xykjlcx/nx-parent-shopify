package com.nx.biz.shopify.repo.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nx.biz.shopify.api.bo.ShopifyOrder;
import com.nx.biz.shopify.convert.ShopifyOrderConverter;
import com.nx.biz.shopify.mapper.ShopifyOrderMapper;
import com.nx.biz.shopify.model.ShopifyOrderDo;
import com.nx.biz.shopify.model.predicate.ShopifyOrderPredicate;
import com.nx.biz.shopify.repo.ShopifyOrderRepository;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Slf4j
@Repository
public class ShopifyOrderRepoImpl implements ShopifyOrderRepository {

    @Resource
    private ShopifyOrderMapper shopifyOrderMapper;
    @Resource
    private ShopifyOrderConverter converter;

    // 将抽象的查询条件，转化为mybatis plus的检索参数
    private LambdaQueryWrapper<ShopifyOrderDo> buildQueryWrapper(ShopifyOrderPredicate predicate) {
        LambdaQueryWrapper<ShopifyOrderDo> queryWrapper = new LambdaQueryWrapper<>();
        if (CheckTools.isNullOrEmpty(predicate)) {
            return queryWrapper;
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqId())) {
            queryWrapper.eq(ShopifyOrderDo::getId, predicate.getEqId());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getNotInFulfillmentStatusList())) {
            queryWrapper.notIn(ShopifyOrderDo::getFulfillmentStatus, predicate.getNotInFulfillmentStatusList());
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getTrackNumberIsNullOrEmpty()) && predicate.getTrackNumberIsNullOrEmpty()) {
            queryWrapper.isNull(ShopifyOrderDo::getTrackNumber);
        }
        return queryWrapper;
    }

    @Override
    public String save(ShopifyOrder shopifyOrderBo) {
        if (CheckTools.isNullOrEmpty(shopifyOrderBo)) {
            throw new RuntimeException("shopifyOrderBo is null");
        }
        ShopifyOrderDo shopifyOrderDo = converter.toDo(shopifyOrderBo);
        if (CheckTools.isNullOrEmpty(shopifyOrderDo.getId())) {
            // 如果id业务没有设置，则生成一个
            shopifyOrderDo.setId(IdUtil.simpleUUID());
        }
        // 调用mybatis落库
        shopifyOrderMapper.insert(shopifyOrderDo);
        return shopifyOrderDo.getId();
    }

    @Override
    public void update(ShopifyOrder shopifyOrderBo) {
        if (CheckTools.isNullOrEmpty(shopifyOrderBo)) {
            throw new RuntimeException("shopifyOrderBo is null");
        }
        if (CheckTools.isNullOrEmpty(shopifyOrderBo.getId())) {
            throw new RuntimeException("shopifyOrderBo.id is null, can not update shopifyOrderBo without id");
        }
        // 由于是业务对象映射到数据对象，所以这一步一定要保证没有信息的丢失
        ShopifyOrderDo shopifyOrderDo = converter.toDo(shopifyOrderBo);
        // 调用mybatis落库
        shopifyOrderMapper.updateById(shopifyOrderDo);
    }

    @Override
    public void deleteById(String dbId) {
        shopifyOrderMapper.deleteById(dbId);
    }

    @Override
    public ShopifyOrder queryById(String dbId) {
        ShopifyOrderDo shopifyOrderDo = shopifyOrderMapper.selectById(dbId);
        if (CheckTools.isNotNullOrEmpty(shopifyOrderDo)) {
            ShopifyOrder shopifyOrderBo = converter.toBo(shopifyOrderDo);
            return shopifyOrderBo;
        }
        return null;
    }

    @Override
    public List<ShopifyOrder> queryList(ShopifyOrderPredicate predicate) {
        LambdaQueryWrapper<ShopifyOrderDo> lambdaQueryWrapper = this.buildQueryWrapper(predicate);
        List<ShopifyOrderDo> shopifyOrderDos = shopifyOrderMapper.selectList(lambdaQueryWrapper);
        if (CheckTools.isNotNullOrEmpty(shopifyOrderDos)) {
            // 映射为boList，这里是否抽离一层逻辑单独实现
            return converter.toBoList(shopifyOrderDos);
        }
        return Collections.emptyList();
    }

}
