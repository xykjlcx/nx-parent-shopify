package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class OrderBoRoot {
    OrderBoV2 order;
}
