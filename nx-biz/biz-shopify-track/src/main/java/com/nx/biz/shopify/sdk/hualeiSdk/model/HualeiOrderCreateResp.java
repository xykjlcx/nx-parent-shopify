package com.nx.biz.shopify.sdk.hualeiSdk.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class HualeiOrderCreateResp {

    @SerializedName("ack")
    private String ack;

    // 系统订单id
    @SerializedName("order_id")
    private String orderId;
    // 原单号
    @SerializedName("order_privatecode")
    private String orderPrivatecode;
    // 转单号
    @SerializedName("order_transfercode")
    private String orderTransfercode;
    // 跟踪号，未传入则自动生成
    @SerializedName("tracking_number")
    private String trackingNumber;

    @SerializedName("attr1")
    private String attr1;
    @SerializedName("attr2")
    private String attr2;
    @SerializedName("channel_code")
    private String channelCode;
    @SerializedName("childList")
    private List<?> childList;
    @SerializedName("delay_type")
    private String delayType;
    @SerializedName("is_changenumbers")
    private String isChangenumbers;
    @SerializedName("is_delay")
    private String isDelay;
    @SerializedName("is_remote")
    private String isRemote;
    @SerializedName("is_residential")
    private String isResidential;
    @SerializedName("label_url")
    private String labelUrl;
    @SerializedName("message")
    private String message;

    @SerializedName("orderpricetrial_amount")
    private String orderpricetrialAmount;
    @SerializedName("orderpricetrial_currency")
    private String orderpricetrialCurrency;
    @SerializedName("post_customername")
    private String postCustomername;
    @SerializedName("product_tracknoapitype")
    private String productTracknoapitype;
    @SerializedName("reference_number")
    private String referenceNumber;
    @SerializedName("return_address")
    private String returnAddress;
}
