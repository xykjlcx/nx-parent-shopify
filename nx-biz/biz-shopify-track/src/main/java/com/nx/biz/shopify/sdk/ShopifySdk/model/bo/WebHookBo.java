package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class WebHookBo {
    Long id;
    String address;
    String topic;
    String created_at;
    String updated_at;
    String format;
    String api_version;
}
