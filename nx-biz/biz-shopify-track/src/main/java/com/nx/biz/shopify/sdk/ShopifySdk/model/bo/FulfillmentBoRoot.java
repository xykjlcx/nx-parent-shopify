package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class FulfillmentBoRoot {
    @SerializedName("fulfillment")
    FulfillmentBo fulfillment;
}
