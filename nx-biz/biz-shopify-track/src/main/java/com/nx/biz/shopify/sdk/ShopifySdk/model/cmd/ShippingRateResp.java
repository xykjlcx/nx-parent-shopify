package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class ShippingRateResp {

    @SerializedName("rates")
    private List<RatesDTO> rates = new ArrayList<>();

    @NoArgsConstructor
    @Data
    public static class RatesDTO {
        @SerializedName("service_name")
        private String serviceName;
        @SerializedName("service_code")
        private String serviceCode;
        @SerializedName("total_price")
        private String totalPrice;
        @SerializedName("description")
        private String description;
        @SerializedName("currency")
        private String currency;
        @SerializedName("min_delivery_date")
        private String minDeliveryDate;
        @SerializedName("max_delivery_date")
        private String maxDeliveryDate;
    }
}
