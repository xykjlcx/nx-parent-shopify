package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class FulfillmentEventBoRoot {

    @SerializedName("fulfillment_event")
    FulfillmentEventBo fulfillmentEvent;
}
