package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

@Data
public class WebHookBoRoot {
    WebHookBo webhook;
}
