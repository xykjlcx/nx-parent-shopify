package com.nx.biz.shopify.convert;

import com.google.gson.Gson;
import com.nx.biz.shopify.api.bo.ShopifyFulfillment;
import com.nx.biz.shopify.model.ShopifyFulfillmentDo;
import com.nx.biz.shopify.sdk.ShopifySdk.model.bo.FulfillmentBo;
import com.nx.common.tools.CheckTools;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ShopifyFulfillmentConverter {

    private static final Gson GSON = new Gson();

    public ShopifyFulfillment toBo(ShopifyFulfillmentDo doItem) {
        ShopifyFulfillment boItem = new ShopifyFulfillment();
        BeanUtils.copyProperties(doItem,boItem);
        return boItem;
    }

    public ShopifyFulfillmentDo toDo(ShopifyFulfillment boItem) {
        ShopifyFulfillmentDo doItem = new ShopifyFulfillmentDo();
        BeanUtils.copyProperties(boItem,doItem);
        return doItem;
    }

    public List<ShopifyFulfillment> toBoList(List<ShopifyFulfillmentDo> doList) {
        List<ShopifyFulfillment> boList = new ArrayList<>();
        for (ShopifyFulfillmentDo doItem : doList) {
            ShopifyFulfillment boItem = this.toBo(doItem);
            // 加入list
            boList.add(boItem);
        }
        return boList;
    }

    public List<ShopifyFulfillmentDo> toDoList(List<ShopifyFulfillment> boList) {
        List<ShopifyFulfillmentDo> doList = new ArrayList<>();
        for (ShopifyFulfillment boItem : boList) {
            ShopifyFulfillmentDo doItem = this.toDo(boItem);
            // 加入list
            doList.add(doItem);
        }
        return doList;
    }

    /* ------------------------------------------------------------------------------------ */

    public ShopifyFulfillment fulfillmentBo2FulfillmentEo(ShopifyFulfillment shopifyFulfillmentBo, FulfillmentBo fulfillmentBo) {
        if (CheckTools.isNullOrEmpty(shopifyFulfillmentBo)) {
            shopifyFulfillmentBo = new ShopifyFulfillment();
            shopifyFulfillmentBo.setId(fulfillmentBo.getId());
        }

        shopifyFulfillmentBo.setName(fulfillmentBo.getName());
        shopifyFulfillmentBo.setOrderId(String.valueOf(fulfillmentBo.getOrder_id()));
        shopifyFulfillmentBo.setStatus(String.valueOf(fulfillmentBo.getStatus()));
        shopifyFulfillmentBo.setShipmentStatus(fulfillmentBo.getShipment_status());
        shopifyFulfillmentBo.setService(String.valueOf(fulfillmentBo.getService()));
        shopifyFulfillmentBo.setLocationId(String.valueOf(fulfillmentBo.getLocation_id()));
        shopifyFulfillmentBo.setEmail(String.valueOf(fulfillmentBo.getEmail()));
        shopifyFulfillmentBo.setTrackingCompany(fulfillmentBo.getTracking_company());
        shopifyFulfillmentBo.setTrackingNumber(String.valueOf(fulfillmentBo.getTracking_number()));
        shopifyFulfillmentBo.setTrackingUrl(String.valueOf(fulfillmentBo.getTracking_url()));
        if (CheckTools.isNotNullOrEmpty(fulfillmentBo.getDestination())) {
            shopifyFulfillmentBo.setDestination(GSON.toJson(fulfillmentBo.getDestination()));
        }
        shopifyFulfillmentBo.setItems(GSON.toJson(fulfillmentBo.getLine_items()));
        shopifyFulfillmentBo.setCreateAt(fulfillmentBo.getCreated_at());
        shopifyFulfillmentBo.setUpdateAt(fulfillmentBo.getUpdated_at());

        return shopifyFulfillmentBo;
    }

}
