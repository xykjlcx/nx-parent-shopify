package com.nx.biz.shopify.repo.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.nx.biz.shopify.api.bo.SyncSettings;
import com.nx.biz.shopify.convert.SyncSettingsConverter;
import com.nx.biz.shopify.mapper.SyncSettingsMapper;
import com.nx.biz.shopify.model.SyncSettingsDo;
import com.nx.biz.shopify.model.predicate.SyncSettingsPredicate;
import com.nx.biz.shopify.repo.SyncSettingsRepository;
import com.nx.common.tools.CheckTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Slf4j
@Repository
public class SyncSettingsRepoImpl implements SyncSettingsRepository {

    @Resource
    private SyncSettingsMapper syncSettingsMapper;
    @Resource
    private SyncSettingsConverter syncSettingsConverter;

    // 将抽象的查询条件，转化为mybatis plus的检索参数
    private LambdaQueryWrapper<SyncSettingsDo> buildQueryWrapper(SyncSettingsPredicate predicate) {
        LambdaQueryWrapper<SyncSettingsDo> queryWrapper = new LambdaQueryWrapper<>();
        if (CheckTools.isNullOrEmpty(predicate)) {
            return queryWrapper;
        }
        if (CheckTools.isNotNullOrEmpty(predicate.getEqId())) {
            queryWrapper.eq(SyncSettingsDo::getId, predicate.getEqId());
        }
        return queryWrapper;
    }

    @Override
    public String save(SyncSettings syncSettingsBo) {
        if (CheckTools.isNullOrEmpty(syncSettingsBo)) {
            throw new RuntimeException("syncSettingsBo is null");
        }
        SyncSettingsDo settingsConverterDo = syncSettingsConverter.toDo(syncSettingsBo);
        if (CheckTools.isNullOrEmpty(settingsConverterDo.getId())) {
            // 如果id业务没有设置，则生成一个
            settingsConverterDo.setId(IdUtil.simpleUUID());
        }
        // 调用mybatis落库
        syncSettingsMapper.insert(settingsConverterDo);
        return settingsConverterDo.getId();
    }

    @Override
    public void update(SyncSettings syncSettingsBo) {
        if (CheckTools.isNullOrEmpty(syncSettingsBo)) {
            throw new RuntimeException("syncSettingsBo is null");
        }
        if (CheckTools.isNullOrEmpty(syncSettingsBo.getId())) {
            throw new RuntimeException("syncSettingsBo.id is null, can not update syncSettingsBo without id");
        }
        // 由于是业务对象映射到数据对象，所以这一步一定要保证没有信息的丢失
        SyncSettingsDo settingsConverterDo = syncSettingsConverter.toDo(syncSettingsBo);
        // 调用mybatis落库
        syncSettingsMapper.updateById(settingsConverterDo);
    }

    @Override
    public void deleteById(String s) {
        syncSettingsMapper.deleteById(s);
    }

    @Override
    public SyncSettings queryById(String s) {
        SyncSettingsDo syncSettingsDo = syncSettingsMapper.selectById(s);
        if (CheckTools.isNotNullOrEmpty(syncSettingsDo)) {
            SyncSettings syncSettingsBo = syncSettingsConverter.toBo(syncSettingsDo);
            return syncSettingsBo;
        }
        return null;
    }

    @Override
    public List<SyncSettings> queryList(SyncSettingsPredicate predicate) {
        LambdaQueryWrapper<SyncSettingsDo> lambdaQueryWrapper = this.buildQueryWrapper(predicate);
        List<SyncSettingsDo> syncSettingsDos = syncSettingsMapper.selectList(lambdaQueryWrapper);
        if (CheckTools.isNotNullOrEmpty(syncSettingsDos)) {
            // 映射为boList，这里是否抽离一层逻辑单独实现
            return syncSettingsConverter.toBoList(syncSettingsDos);
        }
        return Collections.emptyList();
    }

}
