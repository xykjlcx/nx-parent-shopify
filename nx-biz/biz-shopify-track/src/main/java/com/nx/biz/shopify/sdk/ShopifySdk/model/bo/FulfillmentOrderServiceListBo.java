package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

import java.util.List;

@Data
public class FulfillmentOrderServiceListBo {
    List<FulfillmentOrderBo> fulfillment_orders;
}
