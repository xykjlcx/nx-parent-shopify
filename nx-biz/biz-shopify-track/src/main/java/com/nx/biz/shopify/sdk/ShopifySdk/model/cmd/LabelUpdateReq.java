package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import cn.hutool.core.lang.Assert;
import com.nx.common.tools.CheckTools;
import lombok.Data;

@Data
public class LabelUpdateReq {
    String orderId;
    String oldTrackNumber;
    String newTrackCompany;
    String newTrackNumber;
    String transferNo;
    String trackUrl;

    public void check() {
        Assert.isTrue(CheckTools.isNotNullOrEmpty(orderId), "订单id为空");
        Assert.isTrue(CheckTools.isNotNullOrEmpty(oldTrackNumber), "oldTrackNumber为空");
        Assert.isTrue(CheckTools.isNotNullOrEmpty(newTrackNumber), "newTrackNumber为空");
    }
}
