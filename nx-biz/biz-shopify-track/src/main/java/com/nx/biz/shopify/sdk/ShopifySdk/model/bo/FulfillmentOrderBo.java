package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class FulfillmentOrderBo {

    @SerializedName("assigned_location_id")
    private Long assignedLocationId;
    @SerializedName("destination")
    private LocationBo destination;
    @SerializedName("delivery_method")
    private DeliveryMethodDTO deliveryMethod;
    @SerializedName("fulfill_at")
    private String fulfillAt;
    @SerializedName("fulfill_by")
    private String fulfillBy;
    @SerializedName("fulfillment_holds")
    private List<FulfillmentHoldsDTO> fulfillmentHolds;
    @SerializedName("id")
    private Long id;
    @SerializedName("international_duties")
    private InternationalDutiesDTO internationalDuties;
    @SerializedName("line_items")
    private List<LineItemBo> lineItems;
    @SerializedName("order_id")
    private Long orderId;
    @SerializedName("request_status")
    private String requestStatus;
    @SerializedName("shop_id")
    private Long shopId;
    @SerializedName("status")
    private String status;
    @SerializedName("supported_actions")
    private List<String> supportedActions;
    @SerializedName("merchant_requests")
    private List<MerchantRequestsDTO> merchantRequests;
    @SerializedName("assigned_location")
    private LocationBo assignedLocation;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    @NoArgsConstructor
    @Data
    public static class DeliveryMethodDTO {
        @SerializedName("id")
        private Long id;
        @SerializedName("method_type")
        private String methodType;
        @SerializedName("min_delivery_date_time")
        private String minDeliveryDateTime;
        @SerializedName("max_delivery_date_time")
        private String maxDeliveryDateTime;
    }

    @NoArgsConstructor
    @Data
    public static class InternationalDutiesDTO {
        @SerializedName("incoterm")
        private String incoterm;
    }

    @NoArgsConstructor
    @Data
    public static class FulfillmentHoldsDTO {
        @SerializedName("reason")
        private String reason;
        @SerializedName("reason_notes")
        private String reasonNotes;
    }

    @NoArgsConstructor
    @Data
    public static class MerchantRequestsDTO {
        @SerializedName("message")
        private String message;
        @SerializedName("request_options")
        private RequestOptionsDTO requestOptions;
        @SerializedName("kind")
        private String kind;

        @NoArgsConstructor
        @Data
        public static class RequestOptionsDTO {
            @SerializedName("shipping_method")
            private String shippingMethod;
            @SerializedName("note")
            private String note;
            @SerializedName("date")
            private String date;
        }
    }

//    Long id;
//    Long order_id;
//    Long shop_id;
//    Long assigned_location_id;
//    LocationBo destination;
//
//    String request_status;
//    String status;
//
//    List<LineItemBo> line_items;
//    List<String> supported_actions;


}
