package com.nx.biz.shopify.repo;

import com.nx.biz.shopify.api.bo.ShopifySession;
import com.nx.biz.shopify.model.predicate.ShopifySessionPredicate;
import com.nx.common.base.BaseRepository;

public interface ShopifySessionRepository extends BaseRepository<ShopifySession, String, ShopifySessionPredicate> {

}
