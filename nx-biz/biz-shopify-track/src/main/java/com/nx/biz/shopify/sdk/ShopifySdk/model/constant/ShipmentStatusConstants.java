package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

public class ShipmentStatusConstants {
    /**
     FulfillmentEvent->status
         attempted_delivery: Delivery of the shipment was attempted, but unable to be completed.
         carrier_picked_up: A carrier picked up the shipment.
         confirmed: The carrier is aware of the shipment, but hasn't received it yet.
         delayed: The shipment is delayed.
         delivered: The shipment was successfully delivered.
         failure: Something went wrong when pulling tracking information for the shipment. For example, the tracking number was invalid or the shipment was canceled.
         in_transit: The shipment is being transported between shipping facilities on the way to its destination.
         label_printed: A label for the shipment was purchased and printed.
         label_purchased: A label for the shipment was purchased, but not printed.
         out_for_delivery: The shipment is being delivered to its final destination.
         picked_up: The shipment was successfully picked up.
         ready_for_pickup: The shipment is ready for pickup.
     */
    /**
     fulfillment->shipment_status
         label_printed: A label for the shipment was purchased and printed.
         label_purchased: A label for the shipment was purchased, but not printed.
         attempted_delivery: Delivery of the shipment was attempted, but unable to be completed.
         ready_for_pickup: The shipment is ready for pickup at a shipping depot.
         confirmed: The carrier is aware of the shipment, but hasn't received it yet.
         in_transit: The shipment is being transported between shipping facilities on the way to its destination.
         out_for_delivery: The shipment is being delivered to its final destination.
         delivered: The shipment was succesfully delivered.
         failure: Something went wrong when pulling tracking information for the shipment, such as the tracking number was invalid or the shipment was canceled.
     */
    // 已购买，未打印
    public static final String label_purchased = "label_purchased"; //A label for the shipment was purchased, but not printed.
    // 已购买，已打印
    public static final String label_printed = "label_printed"; //A label for the shipment was purchased and printed.
    // 快递公司收到信息
    public static final String confirmed = "confirmed"; //The carrier is aware of the shipment, but hasn't received it yet.
    // 到达提取点
    public static final String ready_for_pickup= "ready_for_pickup"; //The shipment is ready for pickup.
    public static final String carrier_picked_up = "carrier_picked_up"; //A carrier picked up the shipment.
    public static final String picked_up = "picked_up"; //The shipment was successfully picked up.

    public static final String in_transit = "in_transit"; //The shipment is being transported between shipping facilities on the way to its destination.
    public static final String out_for_delivery = "out_for_delivery"; //The shipment is being delivered to its final destination.
    public static final String attempted_delivery = "attempted_delivery"; //Delivery of the shipment was attempted, but unable to be completed.
    public static final String delivered = "delivered"; //The shipment was successfully delivered.

    // 异常
    public static final String delayed = "delayed"; //The shipment is delayed.
    public static final String failure = "failure"; //Something went wrong when pulling tracking information for the shipment. For example, the tracking number was invalid or the shipment was canceled.

}
