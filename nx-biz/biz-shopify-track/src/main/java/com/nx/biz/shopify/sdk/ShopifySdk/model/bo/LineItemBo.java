package com.nx.biz.shopify.sdk.ShopifySdk.model.bo;

import lombok.Data;

import java.util.List;

@Data
public class LineItemBo {
//    Long id;
//    Long shop_id;
//    Long fulfillment_order_id;
//    Long line_item_id;
//    Long inventory_item_id;
//    Long quantity;
//    Long fulfillable_quantity;

    private long id;
    private long variant_id;
    private String title;
    private int quantity;
    private String price;
    private int grams;
    private String sku;
    private String variant_title;
    private String vendor;
    private String fulfillment_service;
    private long product_id;
    private boolean requires_shipping;
    private boolean taxable;
    private boolean gift_card;
    private String name;
    private String variant_inventory_management;
    private List<String> properties;
    private boolean product_exists;
    private int fulfillable_quantity;
    private String total_discount;
    private String fulfillment_status;
    private long fulfillment_line_item_id;

    private TaxLineBo.PriceSet price_set;
    private TaxLineBo.PriceSet total_discount_set;

    private List<TaxLineBo> tax_lines;
    private List<DutyBo> duties;
    private String admin_graphql_api_id;
}



