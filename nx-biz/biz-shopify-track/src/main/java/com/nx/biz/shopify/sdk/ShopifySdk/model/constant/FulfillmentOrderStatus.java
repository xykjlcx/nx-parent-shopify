package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

public class FulfillmentOrderStatus {
    /**
     open: The fulfillment order is ready for fulfillment.
     in_progress: The fulfillment order is being processed.
     scheduled: The fulfillment order is deferred and will be ready for fulfillment after the datetime specified in fulfill_at.
     cancelled: The fulfillment order has been cancelled by the merchant.
     on_hold: The fulfillment order is on hold. The fulfillment process can't be initiated until the hold on the fulfillment order is released.
     incomplete: The fulfillment order cannot be completed as requested.
     closed: The fulfillment order has been completed and closed.
     */
    public static String open = "open";
    public static String in_progress = "in_progress";
    public static String scheduled = "scheduled";
    public static String cancelled = "cancelled";
    public static String on_hold = "on_hold";
    public static String incomplete = "incomplete";
    public static String closed = "closed";
}
