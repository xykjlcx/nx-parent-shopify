package com.nx.biz.shopify.sdk.ShopifySdk.model.cmd;

import cn.hutool.core.lang.Assert;
import com.nx.common.tools.CheckTools;
import lombok.Data;

@Data
public class LabelCanceledReq {
    String orderId;
    String trackNumber;

    public void check() {
        Assert.isTrue(CheckTools.isNotNullOrEmpty(orderId), "订单id为空");
    }
}
