package com.nx.biz.shopify.sdk.ShopifySdk.model.constant;

public class FulfillmentStatus {
    /**
     pending: Shopify has created the fulfillment and is waiting for the third-party fulfillment service to transition it to 'open' or 'success'.
     open: The fulfillment has been acknowledged by the service and is in processing.
     success: The fulfillment was successful.
     cancelled: The fulfillment was cancelled.
     error: There was an error with the fulfillment request.
     failure: The fulfillment request failed.
     */
    public static String pending = "pending";
    public static String open = "open";
    public static String success = "success";
    public static String cancelled = "cancelled";
    public static String error = "error";
    public static String failure = "failure";

}
