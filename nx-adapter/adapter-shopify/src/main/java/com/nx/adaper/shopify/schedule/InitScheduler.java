package com.nx.adaper.shopify.schedule;

import com.nx.biz.shopify.support.ShopifyInitHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化调度器
 */
@Slf4j
@Component
public class InitScheduler {

    @Resource
    private ShopifyInitHelper shopifyInitHelper;

    /**
     * 尝试初始化shopify对接
     * 每30秒执行一次
     */
    @Scheduled(cron = "0/15 * * * * ?")
    public void tryInitShopify() {
        log.info("");
        log.info("");
        log.info("====================================== 定时任务-shopify初始化-[start] ======================================");
        shopifyInitHelper.execInit();
        log.info("====================================== 定时任务-shopify初始化-[end] ======================================");
    }

}
