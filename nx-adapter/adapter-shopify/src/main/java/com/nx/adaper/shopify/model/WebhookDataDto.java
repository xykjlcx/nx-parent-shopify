package com.nx.adaper.shopify.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class WebhookDataDto implements Serializable {

    private String topic;
    private String shopDomain;
    private String payloadJson;

}
