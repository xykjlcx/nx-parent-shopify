package com.nx.adaper.shopify.mq.listener;

import com.google.gson.Gson;
import com.nx.adaper.shopify.model.WebhookDataDto;
import com.nx.biz.shopify.sdk.ShopifySdk.model.constant.TopicConstants;
import com.nx.biz.shopify.service.ShopifyWebhookService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.apis.consumer.ConsumeResult;
import org.apache.rocketmq.client.apis.consumer.MessageListener;
import org.apache.rocketmq.client.apis.message.MessageView;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Slf4j
@Component
public class ShopifyMqMessageListener implements MessageListener {

    private static final Gson GSON = new Gson();

    @Resource
    private ShopifyWebhookService shopifyWebhookService;

    @SneakyThrows
    @Override
    public ConsumeResult consume(MessageView messageView) {
        log.info("");
        log.info("");
        log.info("----------------------------------------------------- shopify-mq消息处理-[start] -----------------------------------------------------");
        Optional<String> messageTagOpt = messageView.getTag();
        if (!messageTagOpt.isPresent()) {
            log.error("~~ 消息没有TAG无法处理，默认返回 ~~");
            log.info("----------------------------------------------------- shopify-mq消息处理-[cancel] -----------------------------------------------------");
            return ConsumeResult.SUCCESS;
        }
        String messageTag = messageTagOpt.get();
        log.info("-> 消息ID：{}", messageView.getMessageId());
        log.info("-> 消息KEY：{}", messageView.getKeys());
        log.info("-> 消息TAG：{}", messageTagOpt.get());

        Charset charset = StandardCharsets.UTF_8;
        String messageBody = charset.decode(messageView.getBody()).toString();
        log.info("-> 消息内容：{}", messageBody);
        WebhookDataDto webhookDataDto = GSON.fromJson(messageBody, WebhookDataDto.class);
        log.info("-> 消息内容序列化为对象：{}", webhookDataDto);


        if (messageTag.equals(TopicConstants.fulfillmentCreate)) {
            shopifyWebhookService.onFulfillmentCreate(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.orderCreate)) {
            shopifyWebhookService.onOrderCreate(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.orderUpdate)) {
            shopifyWebhookService.onOrderUpdate(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.orderDelete)) {
            shopifyWebhookService.onOrderDelete(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.orderPaid)) {
            shopifyWebhookService.onOrderPaid(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.orderFulfilled)) {
            shopifyWebhookService.onOrderFulfill(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else if (messageTag.equals(TopicConstants.fulfillmentRequestCreate)) {
            // todo 这个逻辑上也是不需要的
            shopifyWebhookService.onFulfillmentRequestCreate(webhookDataDto.getPayloadJson(), webhookDataDto.getShopDomain());
        } else {
            log.error("未知的topic {}, payload: {}", messageTag, webhookDataDto.getPayloadJson());
        }
        log.info("----------------------------------------------------- shopify-mq消息处理-[end] -----------------------------------------------------");
        return ConsumeResult.SUCCESS;
    }

}
