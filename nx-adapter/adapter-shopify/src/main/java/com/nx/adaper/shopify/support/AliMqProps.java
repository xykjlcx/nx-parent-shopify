package com.nx.adaper.shopify.support;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "aliyun.mq")
public class AliMqProps {

    private String endpoints;
    private String topic;
    private String consumerGroup;
    private String namespace;
    private String accessKey;
    private String accessSecret;

}
