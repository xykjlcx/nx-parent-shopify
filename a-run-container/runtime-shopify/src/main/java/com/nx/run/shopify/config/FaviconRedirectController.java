package com.nx.run.shopify.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/")
public class FaviconRedirectController {

    /**
     * 默认会请求后台的图标文件，造一个假接口，防止日志报错
     */
    @GetMapping("/favicon.ico")
    public ResponseEntity<byte[]> favicon() {
        return ResponseEntity.noContent().build();
    }

}