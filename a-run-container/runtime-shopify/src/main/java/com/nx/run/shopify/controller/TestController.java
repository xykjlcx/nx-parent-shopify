package com.nx.run.shopify.controller;

import com.nx.common.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/")
public class TestController {

    @GetMapping("/test/renderText")
    public ResponseEntity renderText() {
        return ResponseEntity.ok().body("Hello, World!");
    }

    @GetMapping("/test/renderObj")
    public R<String> renderObj() {
        return R.success("返回对象result");
    }

}
