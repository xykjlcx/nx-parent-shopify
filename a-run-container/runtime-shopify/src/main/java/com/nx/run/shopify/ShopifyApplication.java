package com.nx.run.shopify;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.text.SimpleDateFormat;
import java.util.Date;

@EnableScheduling
@MapperScan(basePackages = {"com.nx.**.mapper"})
@SpringBootApplication(scanBasePackages = {"com.nx.**"})
public class ShopifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopifyApplication.class, args);
        System.out.println(" The system started successfully... \n" +
                " ___       ________     ___    ___ \n" +
                        "|\\  \\     |\\   ____\\   |\\  \\  /  /|\n" +
                        "\\ \\  \\    \\ \\  \\___|   \\ \\  \\/  / /\n" +
                        " \\ \\  \\    \\ \\  \\       \\ \\    / / \n" +
                        "  \\ \\  \\____\\ \\  \\____   /     \\/  \n" +
                        "   \\ \\_______\\ \\_______\\/  /\\   \\  \n" +
                        "    \\|_______|\\|_______/__/ /\\ __\\ \n" +
                        "                       |__|/ \\|__| \n" +
                        "                                   \n" +
                        "                                   ");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        System.out.println("启动成功，当前时间: " + df.format(new Date()));
    }

}
