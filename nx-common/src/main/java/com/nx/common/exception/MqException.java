package com.nx.common.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MqException extends RuntimeException {

    private String msg;

    public MqException(String msg) {
        super(msg);
        this.msg = msg;
    }

}
