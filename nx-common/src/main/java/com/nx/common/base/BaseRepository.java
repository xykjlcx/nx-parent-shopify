package com.nx.common.base;

import java.util.List;

public interface BaseRepository<BO, ID, PD> {
    /**
     * 保存信息
     */
    ID save(BO b);

    /**
     * 更新信息
     */
    void update(BO b);

    /**
     * 删除
     */
    void deleteById(ID id);

    /**
     * 根据id查询
     */
    BO queryById(ID id);

    /**
     * 查询列表
     */
    List<BO> queryList(PD predicate);

}
