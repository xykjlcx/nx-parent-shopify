package com.nx.common.result;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class HttpClientResult implements Serializable {
    private static final long serialVersionUID = -5152302877825031721L;

    /**
     * 响应状态码
     */
    private int code;

    private String reasonPhrase;

    private Map<String, String> responseHeaders = new HashMap<>();
    /**
     * 响应数据
     */
    private String content;

    public int getCode() {
        return code;
    }

    public String getContent() {
        return content;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setReasonPhrase(String reasonPhrase) {
        this.reasonPhrase = reasonPhrase;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public HttpClientResult() {
        super();
    }

    public HttpClientResult(int code) {
        super();
        this.code = code;
    }

    public HttpClientResult(String content) {
        super();
        this.content = content;
    }

    public HttpClientResult(int code, String content) {
        super();
        this.code = code;
        this.content = content;
    }

}
